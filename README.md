# Arriendo-Camas-Backend

Este es un proyecto de ejemplo que utiliza Spring Boot con JPA y Hibernate como capa de persistencia, PostgreSQL como base de datos y un pipeline de CI/CD implementado en GitLab.

## Requisitos previos

Antes de ejecutar este proyecto, asegúrate de tener instalado lo siguiente:

- Java Development Kit (JDK) 8 o superior
- Maven
- PostgreSQL
- GitLab (para la implementación del pipeline de CI/CD)

## Configuración de la base de datos

1. Crea una base de datos en PostgreSQL para el proyecto.
2. Actualiza las credenciales de la base de datos en el archivo `application.properties` ubicado en la carpeta `ms-presupuesto/src/main/resources`. Asegúrate de establecer los valores correctos para las propiedades `spring.datasource.url`, `spring.datasource.username` y `spring.datasource.password`.

## Ejecutar el proyecto

1. Clona este repositorio: `git clone https://ruta-del-repositorio.git`
2. Navega a la carpeta raíz del proyecto: `cd arriendo-camas-backend`
3. Compila el proyecto utilizando Maven: `mvn clean package`
4. Ejecuta la aplicación Spring Boot: `java -jar target/arriendo-camas-backend.jar`
5. La aplicación estará disponible en `http://localhost:8081`

## Pipeline de CI/CD

Este proyecto incluye un pipeline de CI/CD configurado en GitLab. El archivo de configuración `.gitlab-ci.yml` define las etapas del pipeline y las acciones a ejecutar en cada una de ellas.

El pipeline se activará automáticamente en cada push o merge request realizado en la rama `main`. Asegúrate de configurar correctamente las variables de entorno requeridas para la ejecución del pipeline, como las credenciales de la base de datos y cualquier otra configuración específica de tu entorno.