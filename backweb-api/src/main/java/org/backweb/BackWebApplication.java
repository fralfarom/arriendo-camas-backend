package org.backweb;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;

/**
 * Clase Main de la aplicacion.
 *
 * @author Francisco Alfaro
 */
@SpringBootApplication
@ComponentScan
@EnableCaching
@OpenAPIDefinition(info = @Info(title = "API backweb", description = "Servicios Backweb"))
public class BackWebApplication {
    public static void main(String[] args) {
        SpringApplication.run(BackWebApplication.class, args);
    }
}