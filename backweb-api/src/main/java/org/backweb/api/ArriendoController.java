package org.backweb.api;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.backweb.common.constant.Constantes;
import org.backweb.common.messaging.CreatedResponse;
import org.backweb.common.messaging.GetResponse;
import org.backweb.domain.request.ActualizarArriendoRequest;
import org.backweb.domain.request.ContinuarArriendoRequest;
import org.backweb.domain.request.RegistrarArriendoRequest;
import org.backweb.domain.request.RegistrarCamaRequest;
import org.backweb.service.IArriendoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 *Controlador para Arriendos
 *
 * @author FranciscoAlfaro
 */
@Slf4j
@Tag(name = "Controlador para Arriendos", description = "Servicios del controlador Arriendo")
@RestController
@RequestMapping(Constantes.URI_API_ARRIENDOS)
@SecurityRequirement(name = "bearerAuth")
public class ArriendoController {
    @Autowired
    IArriendoService arriendoService;

    /**
     * Permite registrar un nuevo Arriendo.
     * @param request {@link RegistrarCamaRequest}
     *
     * @return {@link CreatedResponse}
     */
    @PostMapping(value = "/")
    @Operation(summary = "Permite registrar un nuevo Arriendo.", method = "POST")
    public @ResponseBody ResponseEntity<CreatedResponse> registrarArriendo(@RequestBody RegistrarArriendoRequest request) {
        return new ResponseEntity<>(arriendoService.registrarArriendo(request), HttpStatus.OK);
    }

    /**
     * Permite teminar un Arriendo.
     * @param idArriendo ID del Arriendo a terminar
     *
     * @return {@link CreatedResponse}
     */
    @PutMapping(value = "/{idArriendo}/terminar")
    @Operation(summary = "Permite terminar un Arriendo.", method = "PUT")
    public @ResponseBody ResponseEntity<CreatedResponse> terminarArriendo(@PathVariable Integer idArriendo) {
        return new ResponseEntity<>(arriendoService.terminarArriendo(idArriendo), HttpStatus.OK);
    }

    /**
     * Permite continuar un Arriendo.
     * @param request {@link ContinuarArriendoRequest} body para continuar un Arriendo.
     *
     * @return {@link CreatedResponse}
     */
    @PostMapping(value = "/continuar")
    @Operation(summary = "Permite continuar un Arriendo.", method = "POST")
    public @ResponseBody ResponseEntity<CreatedResponse> continuarArriendo(@RequestBody ContinuarArriendoRequest request) {
        return new ResponseEntity<>(arriendoService.continuarArriendo(request), HttpStatus.OK);
    }

    /**
     * Paga un Periodo de Arriendo.
     * @param idArriendo ID del Periodo Arriendo a pagar.
     *
     * @return {@link CreatedResponse}
     */
    @PutMapping(value = "/{idArriendo}/pagar")
    @Operation(summary = "Paga un Periodo de Arriendo.", method = "PUT")
    public @ResponseBody ResponseEntity<CreatedResponse> pagarPeriodoArriendo(@PathVariable Integer idArriendo) {
        return new ResponseEntity<>(arriendoService.pagarPeriodoArriendo(idArriendo), HttpStatus.OK);
    }

    /**
     * Permite actualizar un Arriendo.
     * @param request {@link ActualizarArriendoRequest}
     *
     * @return {@link CreatedResponse}
     */
    @PutMapping(value = "/")
    @Operation(summary = "Permite actualizar un Arriendo.", method = "PUT")
    public @ResponseBody ResponseEntity<GetResponse> actualizarArriendo(@RequestBody ActualizarArriendoRequest request) {
        return new ResponseEntity<>(arriendoService.actualizarArriendoById(request), HttpStatus.OK);
    }

    /**
     * Permite obtener todos los Arriendo.
     *
     * @return {@link GetResponse}
     */
    @GetMapping(value = "/")
    @Operation(summary = "Permite obtener todos los Arriendo.", method = "GET")
    public @ResponseBody ResponseEntity<GetResponse> obtenerArriendos() {
        return new ResponseEntity<>(arriendoService.obtenerArriendos(), HttpStatus.OK);
    }

    /**
     * Permite obtener un Arriendo por ID.
     * @param id ID del Arriendo a encontrar.
     *
     * @return {@link GetResponse}
     */
    @GetMapping(value = "/{idArriendo}")
    @Operation(summary = "Permite obtener un Arriendo por ID.", method = "GET")
    public @ResponseBody ResponseEntity<GetResponse> obtenerArriendoById(@Parameter(description = "ID del Arriendo a encontrar.") @PathVariable(name = "idArriendo") Integer id) {
        return new ResponseEntity<>(arriendoService.obtenerArriendoById(id), HttpStatus.OK);
    }

    /**
     * Permite eliminar un Arriendo por ID.
     * @param id ID del Arriendo a eliminar.
     *
     * @return {@link GetResponse}
     */
    @DeleteMapping(value = "/{idArriendo}")
    @Operation(summary = "Permite eliminar un Arriendo por ID.", method = "DELETE")
    public @ResponseBody ResponseEntity<GetResponse> eliminarArriendoById(@Parameter(description = "ID del Arriendo a eliminar.") @PathVariable(name = "idArriendo") Integer id) {
        return new ResponseEntity<>(arriendoService.eliminarArriendoById(id), HttpStatus.OK);
    }


    /**
     * Permite exponer los tipos de Evidencias para un Arriendo.
     *
     * @return {@link GetResponse}
     */
    @GetMapping(value = "/evidencia/tipo-evidencia")
    @Operation(summary = "Permite exponer los tipos de Evidencias para un Arriendo.", method = "GET")
    public @ResponseBody ResponseEntity<GetResponse> obtenerTiposEvidencia() {
        return new ResponseEntity<>(arriendoService.obtenerTiposEvidencia(), HttpStatus.OK);
    }
}
