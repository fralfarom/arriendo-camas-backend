package org.backweb.api;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.backweb.common.constant.Constantes;
import org.backweb.common.messaging.CreatedResponse;
import org.backweb.common.messaging.GetResponse;
import org.backweb.domain.request.ActualizarCamaRequest;
import org.backweb.domain.request.RegistrarCamaRequest;
import org.backweb.service.ICamaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 *Controlador para Camas
 *
 * @author FranciscoAlfaro
 */
@Slf4j
@Tag(name = "Controlador para Camas", description = "Servicios del controlador Camas")
@RestController
@RequestMapping(Constantes.URI_API_CAMAS)
@SecurityRequirement(name = "bearerAuth")
public class CamaController {
    @Autowired
    ICamaService camaService;

    /**
     * Permite registrar una nueva cama.
     * @param request {@link RegistrarCamaRequest}
     *
     * @return {@link CreatedResponse}
     */
    @PostMapping(value = "/")
    @Operation(summary = "Permite registrar una nueva cama.", method = "POST")
    public @ResponseBody ResponseEntity<CreatedResponse> registrarCama(@RequestBody RegistrarCamaRequest request) {
        return new ResponseEntity<>(camaService.registrarCama(request), HttpStatus.OK);
    }

    /**
     * Permite actualizar una Cama.
     * @param request {@link RegistrarCamaRequest}
     *
     * @return {@link CreatedResponse}
     */
    @PutMapping(value = "/")
    @Operation(summary = "Permite actualizar una cama.", method = "PUT")
    public @ResponseBody ResponseEntity<GetResponse> actualizarCama(@RequestBody ActualizarCamaRequest request) {
        return new ResponseEntity<>(camaService.actualizarCamaById(request), HttpStatus.OK);
    }

    /**
     * Permite obtener todas las camas.
     *
     * @return {@link GetResponse}
     */
    @GetMapping(value = "/")
    @Operation(summary = "Permite obtener todas las camas.", method = "POST")
    public @ResponseBody ResponseEntity<GetResponse> obtenerCamas() {
        return new ResponseEntity<>(camaService.obtenerCamas(), HttpStatus.OK);
    }

    /**
     * Permite obtener una Cama por ID.
     * @param id ID de la Cama a encontrar.
     *
     * @return {@link GetResponse}
     */
    @GetMapping(value = "/{idCama}")
    @Operation(summary = "Permite obtener una Cama por ID.", method = "GET")
    public @ResponseBody ResponseEntity<GetResponse> obtenerCamaById(@Parameter(description = "ID de la cama a encontrar.") @PathVariable(name = "idCama") Integer id) {
        return new ResponseEntity<>(camaService.obtenerCamaById(id), HttpStatus.OK);
    }

    /**
     * Permite eliminar una Cama por ID.
     * @param id ID de la Cama a eliminar.
     *
     * @return {@link GetResponse}
     */
    @DeleteMapping(value = "/{idCama}")
    @Operation(summary = "Permite eliminar una Cama por ID.", method = "DELETE")
    public @ResponseBody ResponseEntity<GetResponse> eliminarCamaById(@Parameter(description = "ID de la cama a eliminar.") @PathVariable(name = "idCama") Integer id) {
        return new ResponseEntity<>(camaService.eliminarCamaById(id), HttpStatus.OK);
    }
}
