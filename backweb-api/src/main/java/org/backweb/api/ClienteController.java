package org.backweb.api;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.backweb.common.constant.Constantes;
import org.backweb.common.messaging.CreatedResponse;
import org.backweb.common.messaging.GetResponse;
import org.backweb.domain.request.ActualizarClienteRequest;
import org.backweb.domain.request.RegistrarClienteRequest;
import org.backweb.service.IClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 *Controlador para Clientes
 *
 * @author FranciscoAlfaro
 */
@Slf4j
@Tag(name = "Controlador para Clientes", description = "Servicios del controlador Clientes")
@RestController
@RequestMapping(Constantes.URI_API_CLIENTES)
@SecurityRequirement(name = "bearerAuth")
public class ClienteController {
    @Autowired
    IClienteService clienteService;

    /**
     * Permite registrar un nuevo Cliente.
     * @param request {@link RegistrarClienteRequest}
     *
     * @return {@link CreatedResponse}
     */
    @PostMapping(value = "/")
    @Operation(summary = "Permite registrar un nuevo Cliente.", method = "POST")
    public @ResponseBody ResponseEntity<CreatedResponse> registrarCliente(@RequestBody RegistrarClienteRequest request) {
        return new ResponseEntity<>(clienteService.registrarCliente(request), HttpStatus.OK);
    }

    /**
     * Permite actualizar un Cliente.
     * @param request {@link ActualizarClienteRequest}
     *
     * @return {@link CreatedResponse}
     */
    @PutMapping(value = "/")
    @Operation(summary = "Permite actualizar un Cliente.", method = "PUT")
    public @ResponseBody ResponseEntity<GetResponse> actualizarCliente(@RequestBody ActualizarClienteRequest request) {
        return new ResponseEntity<>(clienteService.actualizarClienteById(request), HttpStatus.OK);
    }

    /**
     * Permite obtener todos los Clientes.
     *
     * @return {@link GetResponse}
     */
    @GetMapping(value = "/")
    @Operation(summary = "Permite obtener todos los Clientes.", method = "GET")
    public @ResponseBody ResponseEntity<GetResponse> obtenerClientes() {
        return new ResponseEntity<>(clienteService.obtenerClientes(), HttpStatus.OK);
    }

    /**
     * Permite obtener un Cliente por ID.
     * @param id ID del Cliente a obtener.
     *
     * @return {@link GetResponse}
     */
    @GetMapping(value = "/{idCliente}")
    @Operation(summary = "Permite obtener un Cliente por ID.", method = "GET")
    public @ResponseBody ResponseEntity<GetResponse> obtenerClienteById(@Parameter(description = "ID del Cliente a encontrar.") @PathVariable(name = "idCliente") Integer id) {
        return new ResponseEntity<>(clienteService.obtenerClienteById(id), HttpStatus.OK);
    }

    /**
     * Permite eliminar un Cliente por ID.
     * @param id ID del Cliente a eliminar.
     *
     * @return {@link GetResponse}
     */
    @DeleteMapping(value = "/{idCliente}")
    @Operation(summary = "Permite eliminar un Cliente por ID.", method = "GET")
    public @ResponseBody ResponseEntity<GetResponse> eliminarClienteById(@Parameter(description = "ID del Cliente a eliminar.") @PathVariable(name = "idCliente") Integer id) {
        return new ResponseEntity<>(clienteService.eliminarClienteById(id), HttpStatus.OK);
    }
}
