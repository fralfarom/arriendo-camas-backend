package org.backweb.config;

import jakarta.validation.ConstraintViolationException;
import lombok.extern.slf4j.Slf4j;
import org.backweb.common.constant.Constantes;
import org.backweb.common.messaging.ErrorResponse;
import org.backweb.exception.BackwebApplicationException;
import org.backweb.exception.BackwebBusinessException;
import org.backweb.exception.BackwebException;
import org.backweb.exception.BackwebExceptionCodes;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Clase para administrar las excepciones.
 */
@RestControllerAdvice
@Slf4j
public class BackwebExceptionController {
    private static final String APP_BUSINESS_EXCEPTION = "[{}][AppBusinessException]";

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<ErrorResponse> handleConstraintViolationException(ConstraintViolationException ex) {
        log.error("[{}][handleConstraintViolationException]", LocalDateTime.now(), ex);
        ErrorResponse genericErrorResponse = new ErrorResponse();
        genericErrorResponse.setCodigo(BackwebExceptionCodes.BACKWEB_GLOB_BIZ_0003.name());
        genericErrorResponse.setMensaje(BackwebExceptionCodes.BACKWEB_GLOB_BIZ_0003.getMessage());
        return ResponseEntity.badRequest().body(genericErrorResponse);
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    public ResponseEntity<ErrorResponse> handleDataIntegrityViolationException(DataIntegrityViolationException ex) {
        log.error("[{}][DataIntegrityViolationException]", LocalDateTime.now(), ex);
        ErrorResponse genericErrorResponse = new ErrorResponse();
        genericErrorResponse.setCodigo(BackwebExceptionCodes.BACKWEB_GLOB_BIZ_0004.name());
        genericErrorResponse.setMensaje(BackwebExceptionCodes.BACKWEB_GLOB_BIZ_0004.getMessage());
        return ResponseEntity.badRequest().body(genericErrorResponse);
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<ErrorResponse> handleAccessDeniedException(AccessDeniedException ex) {
        log.error("[{}][AccessDeniedException]", LocalDateTime.now(), ex);
        ErrorResponse genericErrorResponse = new ErrorResponse();
        genericErrorResponse.setCodigo(BackwebExceptionCodes.BACKWEB_GLOB_SEC_0001.name());
        genericErrorResponse.setMensaje(BackwebExceptionCodes.BACKWEB_GLOB_SEC_0001.getMessage());
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body(genericErrorResponse);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorResponse> handleValidationExceptions(MethodArgumentNotValidException ex) {
        Map<String, String> errorsArgument = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach(error -> {
            String errorMessage = error.getDefaultMessage();
            String fieldName = ((FieldError) error).getField();
            errorsArgument.put(fieldName, errorMessage);
        });

        log.error(APP_BUSINESS_EXCEPTION, LocalDateTime.now(), ex);
        ErrorResponse genericErrorResponse = new ErrorResponse();
        genericErrorResponse.setCodigo(BackwebExceptionCodes.BACKWEB_GLOB_BIZ_0001.name());
        List<String> erroresArgumentsString = errorsArgument
                .entrySet().stream().sorted(Comparator.comparing(Map.Entry::getValue)).map(err -> Constantes.LEFT_BRACKET
                        + "Atributo '" + err.getKey() + "' con error: " + err.getValue() + Constantes.RIGHT_BRACKET)
                .toList();

        genericErrorResponse.setMensaje(String.valueOf(erroresArgumentsString));

        return ResponseEntity.badRequest().body(genericErrorResponse);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public ResponseEntity<ErrorResponse> handleValidationExceptions(HttpRequestMethodNotSupportedException ex) {

        log.error(APP_BUSINESS_EXCEPTION, LocalDateTime.now(), ex);
        ErrorResponse genericErrorResponse = new ErrorResponse();
        genericErrorResponse.setCodigo(BackwebExceptionCodes.BACKWEB_GLOB_BIZ_0001.name());

        genericErrorResponse.setMensaje(ex.getMessage());

        return ResponseEntity.badRequest().body(genericErrorResponse);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(BindException.class)
    public ResponseEntity<ErrorResponse> handleValidationExceptions(BindException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach(error -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });

        log.error(APP_BUSINESS_EXCEPTION, LocalDateTime.now(), ex);
        ErrorResponse genericErrorResponse = new ErrorResponse();
        genericErrorResponse.setCodigo(BackwebExceptionCodes.BACKWEB_GLOB_BIZ_0001.name());
        List<String> errores = errors
                .entrySet().stream().sorted(Comparator.comparing(Map.Entry::getValue)).map(e -> Constantes.LEFT_BRACKET
                        + "Atributo '" + e.getKey() + "' con error: " + e.getValue() + Constantes.RIGHT_BRACKET)
                .toList();

        genericErrorResponse.setMensaje(String.valueOf(errores));

        return ResponseEntity.badRequest().body(genericErrorResponse);
    }


    /**
     * Handler de una excepcion de Negocio
     *
     * @param ex BackwebException
     * @return {@link ErrorResponse}
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(BackwebBusinessException.class)
    public ErrorResponse handleException(BackwebException ex) {
        log.error(APP_BUSINESS_EXCEPTION, LocalDateTime.now(), ex);
        ErrorResponse genericErrorResponse = new ErrorResponse();
        genericErrorResponse.setCodigo(ex.getCode().name());
        genericErrorResponse.setMensaje(ex.toString());

        return genericErrorResponse;
    }

    /**
     * Handler de una excepcion de Aplicacion
     *
     * @param ex BackwebApplicationException
     * @return {@link ErrorResponse}
     */
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(BackwebApplicationException.class)
    public ErrorResponse handleException(BackwebApplicationException ex) {
        log.error("[{}][AppApplicationException]", LocalDateTime.now(), ex);
        ErrorResponse genericErrorResponse = new ErrorResponse();
        genericErrorResponse.setCodigo(ex.getCode().name());
        genericErrorResponse.setMensaje(ex.toString());
        return genericErrorResponse;
    }

    /**
     * Handler de una excepcion no controlada
     *
     * @param ex Exception
     * @return {@link ErrorResponse}
     */
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    public ErrorResponse handleException(Exception ex) {
        log.error("[{}][Exception]", LocalDateTime.now(), ex);
        ErrorResponse genericErrorResponse = new ErrorResponse();
        genericErrorResponse.setCodigo(BackwebExceptionCodes.BACKWEB_GLOB_APP_0001.name());
        genericErrorResponse.setMensaje(BackwebExceptionCodes.BACKWEB_GLOB_APP_0001.toString());

        return genericErrorResponse;
    }
}
