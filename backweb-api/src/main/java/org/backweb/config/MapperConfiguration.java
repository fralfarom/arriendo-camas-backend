package org.backweb.config;

import lombok.extern.slf4j.Slf4j;
import org.backweb.domain.dto.Evidencia;
import org.backweb.domain.dto.PeriodoArriendo;
import org.backweb.persistence.entity.RelPeriodoArriendo;
import org.backweb.persistence.entity.TalEvidencia;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeMap;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Slf4j
public class MapperConfiguration {
    @Bean
    public ModelMapper modelMapper() {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setAmbiguityIgnored(true);

        TypeMap<Evidencia, TalEvidencia> typeMapEvidencia2TalEvidencia= modelMapper.createTypeMap(Evidencia.class, TalEvidencia.class);
        typeMapEvidencia2TalEvidencia.addMappings(mapper -> {
            mapper.map(Evidencia::getId, TalEvidencia::setId);
            mapper.map(Evidencia::getTipoEvidencia, TalEvidencia::setTipoEvidencia);
            mapper.map(Evidencia::getByteImg, TalEvidencia::setImagen);
        });

        TypeMap<RelPeriodoArriendo, PeriodoArriendo> typeMapRelPeriodoArriendo2PeriodoArriendo= modelMapper.createTypeMap(RelPeriodoArriendo.class, PeriodoArriendo.class);
        typeMapRelPeriodoArriendo2PeriodoArriendo.addMappings(mapper -> {
            mapper.map(RelPeriodoArriendo::getFechaInicio, PeriodoArriendo::setFechaInicio);
            mapper.map(RelPeriodoArriendo::getFechaFin, PeriodoArriendo::setFechaFin);
            mapper.map(RelPeriodoArriendo::getEstado, PeriodoArriendo::setEstado);
        });

        return modelMapper;
    }

}
