package org.backweb;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.backweb.common.constant.Constantes;
import org.backweb.domain.dto.Evidencia;
import org.backweb.domain.enumeration.EstadoCama;
import org.backweb.domain.enumeration.TipoEvidencia;
import org.backweb.domain.request.ContinuarArriendoRequest;
import org.backweb.domain.request.RegistrarArriendoRequest;
import org.backweb.persistence.entity.TalCama;
import org.backweb.persistence.entity.TalCliente;
import org.backweb.persistence.repository.ITalCamaRepository;
import org.backweb.persistence.repository.ITalClienteRepository;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Clase de Tests para controlador ArriendoController
 */
@Slf4j
@SpringBootTest(classes = BackWebApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles("test")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class ArriendoControllerTest {
    @Autowired
    MockMvc mockMvc;
    @Autowired
    ITalClienteRepository clienteRepository;
    @Autowired
    ITalCamaRepository camaRepository;
    private TalCliente cliente;
    private TalCama cama;
    private Integer idArriendo;
    private ObjectMapper objectMapper = new ObjectMapper();

    /**
     * Metodo para crear Evidencias necesarias en un Arriendo.
     *
     * @return List {@link Evidencia}
     */
    private List<Evidencia> crearEvidencias() {
        List<Evidencia> listaEvidencias = new ArrayList<>();

        listaEvidencias.add(new Evidencia(
                null,
                "iVBORw0KGgoAAAANSUhEUgAAAhgAAAEPCAIAAACP3Sr5AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAACUOSURBVHhe7Z1fqB3HnedvYg8rsx5wMDPoYRbEMg9hwkAG5yEj9sGDYWdYZhY/OlpY+yF7Z2EfnIfZITAb7ssEsRDWNzhrZb3rP9kNODN6EPEyV6OHXdkkcINgbSmSIuFr+9rYSEKKbF2kWLL+2Pvr/lVVV1dVn3vOPX3uqa7z+fBDqvp1Vd06v+7+fU91n3N66XMAAIApQEgAAGAqEBIAAJgKhAQAAKYCIQEAgKlASAAAYCoQEgAAmAqEBAAApgIhAQCAqUBIAABgKhASAACYCoQEAACmAiEBAICpQEgAAGAqEBIAAJgKhAQAAKYCIQEAgKlASACgZJaeeRPr10xkPRASACiZIAli05uJrAdCAgAlY3Pf+9j0hpAAwCKCkPRoCAkALCIISY+GkADAIoKQ9GgICQAsIghJj4aQAMAikhSSB549qf4RJs3e3dr0e2E2MiEICQCUjFMFZ+OoiJrfazx7eXnpkdWNwDmmbdt3msHf/3xjZf+I7qO3WrNhCUFIAKBkYklQzzjm9xrPMhaSPsyGJQQhAYCSiSVBqt9cvxJY7Ax6jWcICQBAcdjc18qGgWaIifMbP7sUePxetb2+un9JWV6rPGvLprq0/HLdoCvXBx39Zq5cF9ZWtOH+1ddN3+qik3pWml7WKehMgu7i3Fh9pC66odp/aPVAx9bk4MZsMEMQEgAoGZv7WtnQFwy12Bn0UjFo8nvL2jla07Fvawes0qj5zfy+ktdXNsTZ3LGonJrNa2EInUFL013+nFOIqnxgLf5DOp+OrdHgxmwwQxASACgZm/vCbDiO+b3qrKo517M6Zde4LNzKvMbq9/ieCPnN/Aze9JW1TpXNpaNqg9+yPRPTMj2mX44LKWd6cGM2LCEICQCUTCwJoUJ0OENPLCSNRxYrcY4OTa81dSd931mtfhASAIAssLmvlQ39qpo4r9xsfWskahZd2pLlSOJKlMvRCRMtqUewOiHOak3j+trxm2xeObVl96Ut19L96WQ5LnRtjQc3ZoMZgpAAQMnY3Ndkwy+unvSrapFsJPWmSrJKnWorPajYf2B59IqkuQJm87LzLB/wU/nysrlDblK517LjZrv7c/6fTpbjQoczMbgxG8wQhAQASsbmvkRCHG1BF0zMRiYEIQGAkpmHKsi7e5/W1aFBG0ICAIvIPISkWENIAGARQUh6NIQEABYRhKRHQ0gAYBFBSHo0hAQAFhGEpEdDSABgEdHch/VoJrIeCAkAlExX7oMdgJAAwCKCkPQIQgIAiwhC0iMICQAsIghJjyAkALCIICQ9gpAAwCKCkPQIQgIAiwhC0iMICQAsInHu8z1vXv5Eq2JSdlvvWz354A9OSUGbVaxVP+m7f3XDVGs26geSLK+ZavFocEzFAyEBgJIJcp+UnzxWfUPbVT++dVcKqijqFO7c++zarbu+ByERJCCtmFgQEgAomSD3bW59Kv/G2VDkxHfevvdZMmMGICQKQgIAJZPMfUnPyvpFV3740Oln3risVYO3IlH9cCAkCAkAlEwy9/keXYs8/dqHpm4JnU5I6oKqh1dcCJLBFBASACiZZO5zHr01cuTta1oNaHW0QuKLB5e2FIQEAEommfucRwr6YS3Fv1MS3H5HSAQJSCsmFoQEAEomyH1Pv/ahesRUKnyTBvqZLjVfY5yQqHjUN0vMvRKEZMmFDMvTzI4CgB2xg/Noc+vT505dMRWHFRJbbEBIEJLczewoANgRnEc90hXMSkiChylimRgnAMD0cB71SFcwEZJ8bSgngM4Tm5eZ3QAdEKUe6QomQpKvDeUE0Hli8zKzG6ADotQjXcFESPK1oZwAdp7h/LFZ21COkPlClHqkK5gISb42lBPAzjOcPzZrG8oRMl+IUo90BTMhJA88e1JbjzBp9u7Wpt8L691sqHPHHRLYLttQjpD5QpR6pCuYoZCMoyJqfq/t7OXlpUdWNwKnWJd/UuseZ2Nlfz9/osNmOb6Nc+5Mfjxg/dhQjpD5olHCejQTWY9QSPzWo83vtZ0kzE9Ihmw2zrmTOh6w3bChHCHzhSj1SFcwE0LyzfUrgcXOoBdCMguTINdxzh07z9bkuUC6C2bDCKMgSj3SFcxxheQbP7sUePxeze8FLL8s1eb3A+rqNkKytqI/7b9/9XX1p7uvHlCfa1ZfVlLPSjO+dS4tHVhzfetN0bChpRq87p47sLyW9HgvrfnTrvE2M7eTjP9KZRLkOs65Y+fZzJwLpDu0tfpQ2b+yMd5ftDGEURClHukK5kxXJM45IgXUp42UE2eO390m9+pMcwph0u7G6iNL8R9aO1Anbs9jLPYE5hpU+b3J/pXFHtdYClYGmtdSOVMztxPQScqmlLZJkOs4546dZzjzcczv1YpMwrq2ju41vvU1zo5NJtB6J7Gt2RjCKIhSj3QFczb3SPSNVYU6x0oBsiAwZ9Go7rYsyVoVKHCajjVVdvb6hsNGFjSoRtPUby32tP50s8m+lo6Zm79SI5OsPW19qszGOXfi40GqwTsPsdgZ9GqHK7auraN7jW99jbNjm3gCNvIwCqLUI13BTAiJX1WLnZHHOwealCrv39U5Tgqo3uxXyXeb7rYszdJCsm2id8O2LW4QjzbW+JVtJyTBIJXV66rWG1IJch3n3LHzbM080AwxcS7CBVI9kk0T3ZvNaG7/xpMRj8FWw8m0ZlibjTyMgij1SFcwxxWSKzdbN0WjZt5xL+/rw6tV3taWVSePOaWrxvVJu013V6766mnZvrRlBzRm2yeGbVuiQZUR2qPFnsR8mtfSbA1btgcxJi/E90uQ6zjnjp1n80Kk6guGWuwMerXDFTuTW8WqeGZzgTR5hPgHxujJuDFduenuzdCYjTyMgij1SFcwQyH54upJv6oWtEl6zDu16tyw78j2H1geY0WyvFy9ExfMybZNd69cnYF1w/R7SZ2Max8PG1iygXQ32OkFHm8+zZ/2u6fKwSTtC7EJxVjXPssNO89w5uOY36sdrmb/JvJ7y1p+uxYc3d2WZUeoAgVO07GmdRTFw7at6tvaiYHHTi81maQzOUNTRUjSnLt688jb1z6+dVerWUZJDgR9ssnA6ApmKCTu6BxtQZeMTc699ok9HLOhzp34kEgeIbEz8niJssm/IvBxnvXN91fvBqpMvU13W06m6Xbq79jkhm1b3LftQUhmx8ETlx49vLHn2VMaluMfXFd/HKUH6jYP//C0VrXBF6r30IYvHTotHmlm6v2zAEIyM5PTwCc4V2dj8v6xOQ99m8dkJrSufZYbdp6tmftVNXGWf4G0VrKRl7b0SHMT8MtdzniGxmzkFw5Zc7xy/iP519Rrnlh7TwOy9/kzf3bkHfeU3CBKqiLOxDOiqp4ZgJAMxaqTVmideMOyWR7HfWLn2cx8gS+Qikljg6cfSmoCTTnp7JhhbTbyi4KIh7/meOaNy2ZDjSxBjm5uXbxx29QtQZRc1S/UW0zB+SPq7L9qdm+jA+bB7RX2mbt1yzXjF6drYnu1GvjO5WXx1cM0w2b0JN+u4JQrJMO37gM6L+w8E5MfbUGXjE3SegZr1mZBY8yGsUBkPfHS2avu8pQiyqEved8LZx9/9d0jb18zG0YSRCkuVwO++CsxV/XbeNTvczSrV8WwVKd+FYW6ZVQMehlvu5dpWZWboi3Nn67gICT5WvcBnRd2nuH8Z2OS0312Jb+7K1qh7epk6jTTmsZQjpDxWb9ww19zPLH2ntlQc+7qTZEWdxd9TIIoadWZetwm/VfsFxd/I3bikrk+VuMld1euZKBZMYi3rqRatsq+M9WrWY7UNH9hzmhwTMUDIcnXuvZZbth5hvMvweZ/gdReTKsIhWooR0iSza1PZUnxyvmPTL1GhERflN7nkEWJ2TAFQZRcNSiI3f/96n67q6pVfQwpSehBSNwQnrM9bD5EMTEgJPla1z7LDTvPcP7YrG0oR4jPxRu3Zc3x0HO/1Ml/9cfnzYYaWW2IuojGmHofBFFy5Z++s6VlbSB25te3pHr3s8Zz/fa9uq3SJQk25TfZP9nSL1e9jLezV1PJBw2LqXhUQoLlbGZHZYydZ5jmsFlb5keIfrYquCUuUqHT1vscK+sXzYaZ0V+UkpKgSqCktqbLVaG+r15hlx5+S3/YwVzaghzp7wSYLXaeYZrDZm15HiE379zz73OIBXc11i/ciD9bNTvyjNJA6QomQpIvQzkB7DzDNIfN2uZ+hOh3yGVVIeJhXDVf/tE5mZje55Ct/V6qmpS5R6kkuoKJkOTLUE4AnSc2LzO7YXd56tj7/poj+Jzum5c/mfSzVbNjjlEqj65gIiT5MpQTYCjzLI9diLwsJl45/9G3f35h/cIN46rRr5FnsuYYDcdnj3QFEyHJl6GcAEOZZ3nMNPIHT1xyn60SC+6Zi3Ls5n2OaZhplBaNrmAiJPkylBNgKPMsj14ir5+tkjVH8KUN/Rq5+2xV8MNWA4Ljs0e6gomQ5MtQToChzLM8poz8kbev+fc5RDDMhhpZcORzn2MapowS+HQFMxQSv92blz/Rqpj+lKaW71s9+eAPquNPm1WsVT8V4X0EukI/Bp3NB6CHh0bbVDJmKPMsDxf51Rf+1+/+1YsP/c3hP/jeP8ZLB11zyKpClh3GVXP8g+vSXdYcep8juAtSDByfPdIVzJaQSPnJY9VnCl1V35KooqhTuHPvs2v1t4pMXUBIZoBEuBXkXBnKPMtDIy8qsvSdf1j6oz9d+v2vLf3hn+z93mtOS6SgbdREM9Sv3Lxzr4w1x2j0tZsKTEdXMFtCoh+9iNvJ0eY7b9/7LDlWAEIyJRLkceI8d4Yyz/LQyH/p6f9WqYjlwa/88dd/8pY20DPXfbbq6OaW+hcKjs8e6Qpm4h5J0iNHoSs/fOh08BEOf0Xif7FfQEh2jIQ63hcZEs/T93CBdHZoJJf+9v8u/fbD9dlW8U/27PGjOpTPVs0OjZKp1PieCY7PFu2fM5mU6rDu7j56a0C68XTT60aDYyoe2wiJvqN5+rUPTd0SOl0KqAt63ntF2AkS5HjvZEgwTylzgXR3kOiJ/dZ3/nd1Ucvyr/7tcnAJa8HRKJnKNMdni1ll6p6Y1fQkIMmYjBISjWzX02NaHW0K8MWDFDAlEuF472RIME8ukO4aGvlj71z9ne8e1UXJY3/++GN/f77rnF1MNEqm0tvxiZC0GCUkUtC1nuIHOlBvhGQWSIRbQc6V5DyTHi6Q9ouL/NHNrb3Pn5HyQ8/9MnjCB7go+SQ92xyfgj0u6yfu2kztHaz2QK3z+LaP2q2KdSF8fK/bmhw8ING4Nb1eSQZTaAnJ0699qO3EVCp8kwa6JFTzNaZ6LXUU9IXUr8C8JlLAjnFhz5zkPH2PvgXhAmnvJCMPAcko+Z5xj8/6YNRDsc5umqkbZ+1tnHHRayslb7PzpbZGgwckGnvT65lkMIXEimQ0sjB87tQVU3FUL8EPTIOJAkxO1z7LjeQ8nYcLpLMjGXkISEbJeSY4PuVYbJKzHKR1pTpAm8NTvHXFbjW+uBwXUs704AFeYzdSa9g+SQZTmFhIYNfo2me5kZyn80jBX7xygbRHJHqtAEKKZJScRwrjHp8ISfchh5DkS9c+y41gnlwg3TVcSGEEQZR2fnzWB6geivVxqZm6cXp538/jyXJcSDqlEA8ekGjsTa9nXJQCEJJ86dpnubGDeXKBtBeGcoTMl96OT8Eelx03213udsm9qxwXOpyJwQO8xlI0Ted6sx2yYgcnwFwYyjzLg8iPA1Hqka5gIiT5MpQTYCjzLA8iPw5lRckuOgy7vVbvCiZCki9DOQGGMs/yIPLjQJR6pCuYCEm+DOUEGMo8y4PIjwNR6pGuYFZCguVsZkdlzFDmWR5EfhyIUo90BZMVSb64fTb6sUVzp+vYgllD5MeBKPVIVzARknzRfTbisUV9IQMe/+C6qVhW1i/6ZryWRw9vqF28cbvr2IJZQ+THgSj1SFcwEZJ80X024rFFwvqFG6IBYodOXZF0/3H7gXePv/quy/ia9M2Gmn0vnNU/oXbzzj2zocZ/mrdYMLL+RKDY5tanrg02FzO7BDoIwoVNbyayHghJvpjd9rfHRzy2KBAD/Ylsx5hbpRDLzDNvXPZXJIGQOAET+dFBzAbYRYj8OGiUsB7NRNYDIckX3Wf3/6fWY4se+zf/TvK+afH550+svaerjaeOvT8i3bukbzb0StexBbOGyA8XvfF5/1//3T9deTXDG5+TgpDki6aJ/B9bRDqbF0R+oOzCjc9dBiHJF5cmMn9skZsn7DJEfqCMvvE5REIh8Q9N/6cx9VcwtZx6LL73w2FT0T1O9eNl4/2JdMu+Zrh7aLRNJWOGMs/yIPID5b7vtm587mnf+BwiLSGR8k4fiz97IZkWhGRWDGWe5UHkB8rvHTzq3/j8iyf/0r/xOURaQqKf6okPTZET35l6LD5C0j8S5HhfZMhQ5lkeRH6g+Dc+v/Yv/3WGNz4nJXGPJOlZ2eax+HWatg+7dwlbvAbzI5V1s/BJ981v7nf8yr92bpQgGjagaZkeeSBIqON9kSHxPH3PJBdIx8HbuWm2bTCS0VdQR28NSDeebnptNJKmAoMi8xufk7KNkOhaZMzH4psTJHH+uJOnbuZ0ISrVad+1tIOsLdclz2OIPYrzJ0ceDBLkeO9kSDBPKe/0Auk4dO10x7YN5kuf05PoTR5AgP4ZJSR65o/1WPz26SEVs1SQkkG3+s1sWXJ8q6t1mo411XBe33DYgJEjDweJcLx3MiSY5xQXSMdh2/2Y+Y7uc3pB5AHmxSghkYJei1D8RBC8u2yfHpUIVJnf/u9ldL+ZLSfTfdPXEW9q9fQYOfJwkAi3g5wpyXkmPdtdIK12rVLvYH+XuXJdiC6iuq4dV0fdwdTqLk7XxPZp/6HwMqzbmhw8ING430usycgD7D4tIZnysfjm/KjOmXYeqDxa8k6tplz11VOxPt0ap2taY9u7fs2wAaNHHgwu7JmTnKfvGfcCqeyuVkp2+9EvV/vUFFvHVfoQMuO1W8ZFr62UvM3Ol9oaDR6QaNzvcZiMPAwFeV+174WzL529aupDJrEiGc1m+rH41TmzvKzvuuwJpmeNUG1xZ5Q7i7xydaLVDTveTtYjuvbxsAHbjTwQhpImkvN0ngkukIZZ1tuPTdl3VpXq0JCOLV9dqUazR6Jr2eqeLMeFlDM9eIDX2I3UGnZakpGHoSBCIrvPLdMHzcRCMj86ztZyGUqaSM7TeaQw9gXSilpNRiR932nTeTJTp3N9cky/HBdSzvTgAV5jN1Jr2GmR6MUBhKGgQnLwxCVTHzLDEZLOE1A2+KTO6GEylDQRzHOKC6QGm3i9bF3tZN39TcnbXjm1VPm8lqZ7q6U7jJLluJB0SiEePCDR2JteD7iQwhD5Vn2mLOilrTlQn3w9nn5DYShpYgfzTF8grfKtYvOy8ywv+6k8uojatOy4OuoOnqr7yHJc6HAmBg/wGkvRNOVmOxieqt9RISQwW4aSJoYyz/Ig8kPn3NWbwXOABgpCki9DSROksxq76DAkL3b1DJGHTEBI8mUoaYJ0Ni+IPGQCQpIvQ0kTpLN5QeQhExCSfBlKmiCdzQsiD5mAkOTLUNIE6WxeEPlBs++Fs0N/MKIDIcmXoaQJ0tm8IPKDZs+zO3iMQqZkKCTep+8Xm6GkiaHMszyI/KCRfSdaYioDJxSSB2qRfPiHp7WqR+oXVk9qVfjSodPikWam3j8IiUGDbyoZM5R5lgeRHy4379yTfTf0J+w6WkKiKuJMPCOq6pkBCIlhlkHuk6HMszyI/HDZ3PpU9l2ZQuKOS79QbzEF54+os/+q+x0IqwPNz0j4v3oxxgMhwgdOVM76hzHqYZphd+NrX/OiO9p5Ec/T9/g/t6W/rKXl7R61O937ieoI6e4+emtAuvF00+sJjaSpwNCQRcn6hRumMnBCITElW5Z/9734KzFX9dt4yHlls3pVDEve2Vi3jIpBL+Nt9zItq3JTtKUC6Y52XgTzlHIfj9r19nKOZDE9iV53AAF2j8SKxJl63Cb9V+wXF38jduJS8GArd17ZciUDzYpBvHUl1bJV9p2pXtWwHs1fKA2NtqlkTDDPnh612zoM8iOL6QWRB5gX21/aUrv/+9X9dldVq/oY/PPKlnsQEjeE52wPWzBRkDMlOc+kZ7tH7TbvEroecWZ3fH08lPLE3B2TjDzA7pO+tPXTd7a0rEeq2Jlf35Lq3c8az/Xb9+q2ineytc5he3JWZ6AWky39ctXLeDt7NZWC0TibSsYk5+l7xn3Ubr1ndX/XGbs5HryjqHHGRa+tlLzNzpfaGg0ekGjsTW+eJCMPsPu0hGQK3MnWLtcnXE1qa7pcFaIHTvgt/WGbFuUxlDSRnKfz6K2RcR+12+xku8erfd3sY/HWFf94SJbjQsqZHjzAa+xGag07N5KRh0Hw0tmre58/U8bDSIS+hAT6ZyhpIjlP55GC/xjEUY/aRUgmRKLXCiAMh4MnLsm+K+OB7QJCki9DSRPBPKd41K5kZ5PKqwzfJH2b35u87+fxZDkuJJ1SiAcPSDT2pjdPXEhhcJT0wHYBIcmXoaSJHcwz/ahdocrVFR03213udsm9qxwXOpyJwQO8xlI0TTO62f7S2av/8fsv/u5fvfjQ3xz+g+/947mrN81myJhv//yC7jtTHzgISb5omjCVjBnKPMtDI/9bT/3npe/8w9If/enS739t6Q//ZO/3XkNL8qekB7YLCEm+aJowlYwZyjzHwy46DMmLXbmgkd/zHw5VKmJ58Ct/XMyPk5dNMQ9sFxCSfBlKgh7KPMtDI3/fd48v/fbDRkaWlvbs2cPugF0GIckXTROmkjFDmWd5aOR/7+DR6qKW5S+e/MtifgoQhgJCki9DSdBDmWd5aOSPvXP1d757VBclj/3544/9/Xn91s7NO/cOJT/RANA3CEm+DCVBD2We5eEif3Rza+/zZ6T80HO/fOX8R7r1mTcui+fLPzp3/IPr6gGYEQhJvrg0kTlDmWd5jI68rEv2vXBW2zyx9l4x93XLQHeNqQyfuQpJ9RH++X8YP1s0BZhKxug8sXmZ2Q0pbt65d/DEJVmmSLP2dz9hzpT0wHYhFJIMHrULBg2+qWSMzhObl5nd0I2sRbp+6wzmhew4EXhTGT4tIcnjUbtgIMgwI2R1wpcW50hhD2wXWkLiMpdfqLeYgvPHNN/jar7C1fz6hPUFnuQvVbjG9dbwSRILxIhoA0zDV398Xg6tb//8gj68EnaZwh7YLoRCYkqecoz3qF2H04b4BkjscY0rGTL60bSqnMbrbV8cxog2wMTI2+Fv2R/WLOmXzGGOJFYkztTjNum/YqlH7Zq0X1PLQKUH7dQfe5yQtDeJt67YrcbnyouCRttUAHrlzcufPHp4Qw4wWZ0YF8BO2f7Slto2j9ptlEBKCEk/hEEG6Jsjb19bv3DDVAB2SvrS1sSP2nV5vpKEsGSJPa6bFKySNKKCkCAksNsc/+D6zTv+U7QBtqclJFNQpf/6stbycvMYuUoeFLvcCDyePNgBvNvqCAlCArvKm5c/2fPsqS//6NzRzS3jAhiDvoQE+gchgV1m/cINURE98B5/9V2+DD8jXjp7VSJczHN2BYQkX/R8NhWAXUG/DK/fuy4p02VFYQ9sFxCSfEFIYF7IWuSpY+9zs2RG6APbERLYDRASyAcRlc2tT00FpkMf2P7MG5dNffggJPmCkEA+SNbb8+wpvgzfC4U9sF1ASPIFIYF80DfRYnufP8PzsiAAIckXhASywn0ZXowfpQefSkiwnM3sKIA8eOX8RwdPXDIVgBqEJHczOwogV/i6CSyZ/wEAdsRXf3xejN/sWmQQEgDYObIc2fv8GV0982T4MdEHtpf0cWqEBACm4uadeyvrF/XL8I8eXrBfxNsRGquSvu+JkABAD8j768dffZcLXOOgCzhTKQKEBABmBd9ejJGFiKiILEpMvQgQEgCYCfqj9HwZPqC8B7YLCAkAzIRn3ris13D2Pn/mlfMfGS+UCEICALNi/cKNr//kLZWTkn5aCgIQEgCYLYdOXXn08AY/Sl8wCAkA7DaISmEgJACw23z75xf4MnxJICQAsNvoV7vFnjr2/qJ9Gf6/nroiL/zA2numXgQICQDsNjfv3JNFiX7Be+/zZxbnStfHt+7+8xfP/ou/e+sr//NcSZ+KRkgAYD6cu3rzz468863XPjT10hHlePTwxn+pn7CrD3cpRksQEgCAmaMq4j8QrCQtQUgAICO+/pO3Dp64VNjFrlhFlGK0BCEBgFw48vY1vQm/74WzUjbe4SNqcfyD66bSRvyy1VQGC0ICABmxfuHGV398XuWkmNsnXSuPrpXK4EBIACA7Dp268tBzvywgwzpiLSlGRQSEBABypLzPBPtaUpKKCAgJAAwAyblf/8lbQ8+8qiWbW5+WpCICQgIAA+CpY+/rjZN//38+8C8QDY7/fvrX/+x/nPl/l35j6kWAkADAABDxcF+Gn+j2icoPFpsJUB8gJAAwGPTL8F/+0bnx76AE2RNzZgLUBwgJAAyMiS5t2aT5PuYMIQEAaHHwxKWV9YtdaxSEJDaEBACgQVYneuNk3wtnj25uGa8HQhIbQgIA0OL4B9fdl+Eff/Vd47XYpBkm00U2G5PeQEgAoAT0y/Dxr6ogJLEhJAAAaT6+dTe+U5IUkgeePan+ESbN3t3a9HsVY/YF9gZCAgDFsn7hhlMFZ+OoiJrfazx7eXnpkdWNwBnYOG26bWNl/4juo7das6+uNxASACiTo5tbskCJJUE945jfazybvZD0YfbV9QZCAgAlE0uCVL+5fiWw2Bn0Gs8QEgCA4rBJs5VGA80QE+c3fnYp8Pi9ant9df+SsrxWedaWTXVp+eW6QbdIVBedKvavrjRtrFPQAc0Ia8Yvzo3VR+qidHy9aVB1rwurBzq2Jgc3ZmPSGwgJAJSMTZqtNOoLhlrsDHqpith8HVg7uSeERPwmm9fC4BrbFN/c26icS/tXNsS5VomE+YtV+cCaGcprqQLWsTUa3JiNSW8gJABQMjZphml0HPN71elYk7Vnda6vcem7lbKNSV/Vhspsm/aAsrip874/QrIcF1LO9ODG7KvrDYQEAEomloRQITqcoScWksYji5U4uXuGkAAADBebNFtp1K+qifPKzda3RqJm0aUtWY6oPFRZO07uvonfpPLuS1ua9/0RkuW40LU1HtyYjUlvICQAUDI2aTZp9IurJ/2qWiQbSb2psrNS52h7733/geXRKxIxexGs42a76+WPkCzHhQ5nYnBjNia9gZAAQMnYpJnIpKMt6FKS2RfYGwgJAJTMPFRBlgU+rctKORhCAgAwAfMQktwNIQEAmACEJDaEBABgAhCS2BASAIAJQEhiQ0gAACYAIYkNIQEAmABNmlhsJkB9gJAAQMn0njQLACEBAJgAhCQGIQEAmACEJAYhAQCYAIQkBiEBAJgAhCQGIQEAmACEJAYhAQCYAIQkBiEBAJiAOGn6njcvf6JVMSm7rfetnnzwB6ekoM0q1qqf9N2/umGqNRv1A0mW10x1KOhrNJU+QEgAoGSCpCnlJ49VX+121Y9v3ZWCKoo6hTv3Prt2667vQUhGgJAAQMkESXNz61P5N06jIie+8/a9z8ZJtQiJgpAAQMkkk2bSs7J+0ZUfPnT6mTcua9XgrUhUPxwICUICACWTTJq+R9ciT7/2oalbQqcTkrqg6uEVh0QyJtOAkABAySSTpvPorZEjb1/TakCroxUSXzy4tKUgJABQMsmk6TxS0A9rKf6dkuD2O0IyAoQEAEomSJpPv/ahesRUKnyTBvqZLjVfY5yQqHjUN0vMvRKEBCEBgJLZQdLc3Pr0uVNXTMVhhcQWGxAShAQASqb3pFkACAkAwAQgJDEICQDABCAkMQgJAMAEICQxCAkAwAQgJDEICQDABCAkMQgJAMAEICQxCAkAwARo0sRiMwHqA4QEAEqm96RZAAgJAMAEICQxCAkAwAQgJDEICQDABCAkMQgJAMAEICQxCAkAwAQgJDEICQDABCAkMQgJAMAEICQxCAkAwATESfOBZ0+J5+EfntaqNvjC6kmtCl86dFo80szUi0Nfsqn0AUICACUTJE1VEWfiGVFVT3n0/tIQEgAomSBpuqpfqLeYgvNHrC0v7V9dNU/Z1WfuqtdgnrhbN1vTp7lXPvNgd69L45rTU3q7X+MOQUgAoGSCpBmX5d99L/5KzFX9Nh61ZGjmr4qBBtT64R7oHhW9Lq5lVWzUZRfpfo07BCEBgJIJkqZWnanHbdJ/xX5x8TdiJy59optqPAFoi0G1tKhQT0czV26WIzXzWJPoazSVPkBIAKBkgqTpqkFB7P7vV/fbXVWt6mPoUgWVAinp1nGEZB7q4RG9tGlBSACgZIKk6co/fWdLy9pA7Myvb0n17meN5/rte3VbJaUKzlfJg5ZSzVplKTTeuaCvzlT6ACEBgJLpL2kmVcFeqNq/vDzmikSwnSq4tAUAkDm9J80CQEgAACYAIYlBSAAAJgAhiUFIAAAmACGJQUgAACYAIYlBSAAAJgAhiUFIAAAmACGJQUgAACZAkyYWmwlQHyAkAFAyQfbEnJkA9QFCAgAAU4GQAADAVCAkAAAwFQgJAABMBUICAABTgZAAAMyQizduP3p444m190y9PD7//P8De8dGY9i0J3oAAAAASUVORK5CYII=",
                TipoEvidencia.INSTALACION.toString(),
                null));

        return listaEvidencias;
    }

    /**
     * Metodo para armar el request necesario para continuar un Arriendo.
     *
     * @param idArriendo
     * @return {@link ContinuarArriendoRequest}
     */
    private ContinuarArriendoRequest armarContinuarArriendoRequest(Integer idArriendo) {
        return new ContinuarArriendoRequest(
          idArriendo,
          new Date(),
          new Date()
        );
    }

    /**
     * Metodo para armar el request necesario para registrar un Arriendo.
     *
     * @param evidencias
     * @return {@link RegistrarArriendoRequest}
     */
    private RegistrarArriendoRequest armarArriendoRequest (List<Evidencia> evidencias) {
        return new RegistrarArriendoRequest(
                cliente.getId(),
                cama.getId(),
                70000,
                "Las Hortensias 8735",
                new Date(),
                new Date(),
                evidencias
        );
    }

    /**
     * Metodo para poblar tal_cliente para llevar a cabo los test
     */
    private void poblarTalCliente() {
        cliente = new TalCliente(
                null,
                "11111111-1",
                "Francisco Alfaro",
                "+569334455764"
        );

        cliente = clienteRepository.save(cliente);
        log.info("[ArriendoControllerTest][poblarTalCliente] Cliente: " + cliente);
    }

    /**
     * Metodo para poblar tal_cama para llevar a cabo los test
     */
    private void poblarTalCama() {
        cama = new TalCama(
                null,
                "PRUEBACAMA",
                EstadoCama.DISPONIBLE.toString()
        );

        cama = camaRepository.save(cama);
        log.info("[ArriendoControllerTest][poblarTalCama] Cama: " + cama);
    }

    @BeforeAll
    void setup() {
        //Poblar DB con datos necesarios
        poblarTalCliente();
        poblarTalCama();
    }

    @Test
    @Order(1)
    void registrarArriendo () throws Exception{
        List<Evidencia> evidencias = crearEvidencias();
        RegistrarArriendoRequest requestArriendo = armarArriendoRequest(evidencias);

        //Mockear GET Request
        ResultActions response = mockMvc.perform(
                MockMvcRequestBuilders.post(Constantes.URI_API_ARRIENDOS + Constantes.SLASH)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(new ObjectMapper().writeValueAsString(requestArriendo))
        );

        //Verificar Status OK
        response.andExpect(status().isOk());

        String responseBody = response.andReturn().getResponse().getContentAsString();
        assertNotNull(responseBody);
        assertFalse(responseBody.isEmpty());

        JsonNode jsonResponse = objectMapper.readTree(responseBody);

        //Validar Respuesta con un campo en especifico
        JsonNode campoValue = jsonResponse.get("id");
        assertNotNull(campoValue);

        idArriendo = Integer.parseInt(campoValue.toString());
    }

    @Test
    @Order(2)
    void obtenerArriendos () throws Exception{
        //Mockear GET Request
        ResultActions response = mockMvc.perform(
                MockMvcRequestBuilders.get(Constantes.URI_API_ARRIENDOS + Constantes.SLASH)
        );

        //Verificar Status OK
        response.andExpect(status().isOk());

        String responseBody = response.andReturn().getResponse().getContentAsString();
        assertNotNull(responseBody);
        assertFalse(responseBody.isEmpty());

        JsonNode jsonResponse = objectMapper.readTree(responseBody);

        //Validar Respuesta con un campo en especifico
        JsonNode campoValue = jsonResponse.get("data");
        assertNotNull(campoValue);
    }

    @Test
    @Order(3)
    void obtenerArriendoById () throws Exception{
        //Mockear GET Request
        ResultActions response = mockMvc.perform(
                MockMvcRequestBuilders.get(Constantes.URI_API_ARRIENDOS + Constantes.SLASH + idArriendo)
        );

        //Verificar Status OK
        response.andExpect(status().isOk());

        String responseBody = response.andReturn().getResponse().getContentAsString();
        assertNotNull(responseBody);
        assertFalse(responseBody.isEmpty());


        JsonNode jsonResponse = objectMapper.readTree(responseBody);

        //Validar Respuesta con un campo en especifico
        JsonNode campoValue = jsonResponse.get("data");
        assertNotNull(campoValue);
    }

    @Test
    @Order(4)
    void pagarPeriodoArriendo () throws Exception{
        ResultActions response = mockMvc.perform(
                MockMvcRequestBuilders.put(Constantes.URI_API_ARRIENDOS + Constantes.SLASH + idArriendo + Constantes.SLASH + "pagar")
        );

        //Verificar Status OK
        response.andExpect(status().isOk());

        String responseBody = response.andReturn().getResponse().getContentAsString();
        assertNotNull(responseBody);
        assertFalse(responseBody.isEmpty());

        JsonNode jsonResponse = objectMapper.readTree(responseBody);

        //Validar Respuesta con un campo en especifico
        JsonNode campoValue = jsonResponse.get("id");
        assertNotNull(campoValue);
    }

    @Test
    @Order(5)
    void continuarArriendo () throws Exception{
        ResultActions response = mockMvc.perform(
                MockMvcRequestBuilders.post(Constantes.URI_API_ARRIENDOS + Constantes.SLASH + "continuar")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(armarContinuarArriendoRequest(idArriendo)))
        );

        //Verificar Status OK
        response.andExpect(status().isOk());

        String responseBody = response.andReturn().getResponse().getContentAsString();
        assertNotNull(responseBody);
        assertFalse(responseBody.isEmpty());

        JsonNode jsonResponse = objectMapper.readTree(responseBody);

        //Validar Respuesta con un campo en especifico
        JsonNode campoValue = jsonResponse.get("id");
        assertNotNull(campoValue);
    }

    @Test
    @Order(6)
    void terminarArriendo () throws Exception{
        ResultActions response = mockMvc.perform(
                MockMvcRequestBuilders.put(Constantes.URI_API_ARRIENDOS + Constantes.SLASH + idArriendo + Constantes.SLASH + "terminar")
        );

        //Verificar Status OK
        response.andExpect(status().isOk());

        String responseBody = response.andReturn().getResponse().getContentAsString();
        assertNotNull(responseBody);
        assertFalse(responseBody.isEmpty());

        JsonNode jsonResponse = objectMapper.readTree(responseBody);

        //Validar Respuesta con un campo en especifico
        JsonNode campoValue = jsonResponse.get("id");
        assertNotNull(campoValue);
    }

    @Test
    @Order(7)
    void actualizarArriendo (){}

    @Test
    @Order(8)
    void eliminarArriendoById () throws Exception{
        //Mockear GET Request
        ResultActions response = mockMvc.perform(
                MockMvcRequestBuilders.delete(Constantes.URI_API_ARRIENDOS + Constantes.SLASH + idArriendo)
        );

        //Verificar Status OK
        response.andExpect(status().isOk());

        String responseBody = response.andReturn().getResponse().getContentAsString();
        assertNotNull(responseBody);
        assertFalse(responseBody.isEmpty());

        JsonNode jsonResponse = objectMapper.readTree(responseBody);

        //Validar Respuesta con un campo en especifico
        JsonNode campoValue = jsonResponse.get("data");
        assertNotNull(campoValue);
    }

    @Test
    void obtenerTiposEvidencia () throws Exception{
        //Mockear GET Request
        ResultActions response = mockMvc.perform(
                MockMvcRequestBuilders.get(Constantes.URI_API_ARRIENDOS + Constantes.SLASH + "evidencia" + Constantes.SLASH + "tipo-evidencia")
        );

        //Verificar Status OK
        response.andExpect(status().isOk());

        String responseBody = response.andReturn().getResponse().getContentAsString();
        assertNotNull(responseBody);
        assertFalse(responseBody.isEmpty());

        JsonNode jsonResponse = objectMapper.readTree(responseBody);

        //Validar Respuesta con un campo en especifico
        JsonNode campoValue = jsonResponse.get("data");
        assertNotNull(campoValue);
    }
}
