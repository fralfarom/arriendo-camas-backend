package org.backweb;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.backweb.common.constant.Constantes;
import org.backweb.domain.request.RegistrarCamaRequest;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Clase de Tests para controlador CamaController
 */
@Slf4j
@SpringBootTest(classes = BackWebApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles("test")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class CamaControllerTest {
    @Autowired
    MockMvc mockMvc;
    private Integer idCama;
    private ObjectMapper objectMapper = new ObjectMapper();

    /**
     * Metodo para armar el request necesario para registrar una Cama.
     *
     * @return {@link RegistrarCamaRequest}
     */
    private RegistrarCamaRequest armarRegistrarCamaRequest () {
        return new RegistrarCamaRequest("CAMAPRUEBA");
    }

    @Test
    @Order(1)
    void registrarCama () throws Exception{
        RegistrarCamaRequest request = armarRegistrarCamaRequest();

        //Mockear GET Request
        ResultActions response = mockMvc.perform(
                MockMvcRequestBuilders.post(Constantes.URI_API_CAMAS + Constantes.SLASH)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(new ObjectMapper().writeValueAsString(request))
        );

        //Verificar Status OK
        response.andExpect(status().isOk());

        String responseBody = response.andReturn().getResponse().getContentAsString();
        assertNotNull(responseBody);
        assertFalse(responseBody.isEmpty());

        JsonNode jsonResponse = objectMapper.readTree(responseBody);

        //Validar Respuesta con un campo en especifico
        JsonNode campoValue = jsonResponse.get("id");
        assertNotNull(campoValue);

        idCama = Integer.parseInt(campoValue.toString());
    }

    @Test
    @Order(2)
    void obtenerCama () throws Exception{
        //Mockear GET Request
        ResultActions response = mockMvc.perform(
                MockMvcRequestBuilders.get(Constantes.URI_API_CAMAS + Constantes.SLASH)
        );

        //Verificar Status OK
        response.andExpect(status().isOk());

        String responseBody = response.andReturn().getResponse().getContentAsString();
        assertNotNull(responseBody);
        assertFalse(responseBody.isEmpty());

        JsonNode jsonResponse = objectMapper.readTree(responseBody);

        //Validar Respuesta con un campo en especifico
        JsonNode campoValue = jsonResponse.get("data");
        assertNotNull(campoValue);
    }

    @Test
    @Order(3)
    void obtenerCamaById () throws Exception{
        //Mockear GET Request
        ResultActions response = mockMvc.perform(
                MockMvcRequestBuilders.get(Constantes.URI_API_CAMAS + Constantes.SLASH + idCama)
        );

        //Verificar Status OK
        response.andExpect(status().isOk());

        String responseBody = response.andReturn().getResponse().getContentAsString();
        assertNotNull(responseBody);
        assertFalse(responseBody.isEmpty());


        JsonNode jsonResponse = objectMapper.readTree(responseBody);

        //Validar Respuesta con un campo en especifico
        JsonNode campoValue = jsonResponse.get("data");
        assertNotNull(campoValue);
    }

    @Test
    @Order(4)
    void actualizarCama (){}

    @Test
    @Order(5)
    void eliminarCamaById () throws Exception{
        //Mockear GET Request
        ResultActions response = mockMvc.perform(
                MockMvcRequestBuilders.delete(Constantes.URI_API_CAMAS + Constantes.SLASH + idCama)
        );

        //Verificar Status OK
        response.andExpect(status().isOk());

        String responseBody = response.andReturn().getResponse().getContentAsString();
        assertNotNull(responseBody);
        assertFalse(responseBody.isEmpty());

        JsonNode jsonResponse = objectMapper.readTree(responseBody);

        //Validar Respuesta con un campo en especifico
        JsonNode campoValue = jsonResponse.get("data");
        assertNotNull(campoValue);
    }
}
