package org.backweb.common.constant;

/**
 * Clase Constantes para declarar constantes en la aplicacion.
 *
 * @author Francisco Alfaro
 */
public class Constantes {

	private Constantes() {
		super();
	}

	public static final String BASE_URI_API = "/api/backweb";
	public static final String URI_API_CAMAS = "/camas";
	public static final String URI_API_ARRIENDOS = "/arriendos";
	public static final String URI_API_CLIENTES = "/clientes";
	public static final String AUTHORIZATION = "Authorization";
	public static final String BEARER = "Bearer ";
	public static final String BLANK = "";
	public static final String COLON = ":";
	public static final String DOT = ".";
	public static final String SPACE = " ";
	public static final String COMMA = ",";
	public static final String LEFT_CURLY_BRACKET = "{";
	public static final String RIGHT_CURLY_BRACKET = "}";
	public static final String LEFT_BRACKET = "[";
	public static final String RIGHT_BRACKET = "]";
	public static final String SLASH = "/";
	public static final Integer PAGINATION_MAX_SIZE = 3000;
	public static final Integer PAGINATION_PAGE = 0;
	public static final String PAGINATION_URL_PAGE = "?page=";
	public static final String PAGINATION_URL_SIZE = "&size=";
}

