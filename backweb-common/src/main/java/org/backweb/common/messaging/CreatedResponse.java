package org.backweb.common.messaging;

import lombok.*;

/**
 * Clase que describe CreatedResponse utilizada para respuestas hacia el cliente.
 *
 * @author Francisco Alfaro
 */
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class CreatedResponse {
	
	private Integer id;
	private String fechaRegistro;

}
