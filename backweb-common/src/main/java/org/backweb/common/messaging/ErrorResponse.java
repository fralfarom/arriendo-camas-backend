package org.backweb.common.messaging;

import lombok.*;

/**
 * Clase que describe ErrorResponse utilizada para respuestas hacia el cliente.
 *
 * @author Francisco Alfaro
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ErrorResponse {

	private String codigo;
	private String mensaje;
	
}
