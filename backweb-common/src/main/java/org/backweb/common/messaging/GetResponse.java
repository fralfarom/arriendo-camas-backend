package org.backweb.common.messaging;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Clase que describe GetResponse utilizada para respuestas hacia el cliente.
 *
 * @author Francisco Alfaro
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class GetResponse {

    private Object data;
    private Pagination pagination;

    public GetResponse(Object data) {
        super();
        this.data = data;
    }
}
