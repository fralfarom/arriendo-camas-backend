package org.backweb.common.messaging;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Clase utlizada para paginar datos.
 *
 * @author Francisco Alfaro
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class Pagination {

    private Integer page;
    private Integer size;
    private Integer totalPage;
    private String previousPage;
    private String nextPage;
    private String uri;
    private Long totalElements;

    public Pagination(Integer page, Integer size, Integer totalPage, String previousPage, String nextPage, Long totalElements) {
        this.page = page;
        this.size = size;
        this.totalPage = totalPage;
        this.previousPage = previousPage;
        this.nextPage = nextPage;
        this.totalElements = totalElements;
    }
}
