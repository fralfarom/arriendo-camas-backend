package org.backweb.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Clase DTO para mapear TalArriendo
 *
 * @author Francisco Alfaro
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Arriendo {
    private Integer id;
    private Cliente cliente;
    private Cama cama;
    private Integer monto;

    private String direccion;
}
