package org.backweb.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Clase DTO para mapear TalCama
 *
 * @author Francisco Alfaro
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Cama {
    private Integer id;
    private String codigo;

    private String estado;
}
