package org.backweb.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Clase DTO para mapear TalCliente
 *
 * @author Francisco Alfaro
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Cliente {
    private Integer id;
    private String rut;
    private String nombre;
    private String telefono;
}
