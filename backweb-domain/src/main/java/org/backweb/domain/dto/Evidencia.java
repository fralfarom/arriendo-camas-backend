package org.backweb.domain.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.Parameter;
import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Clase DTO para mapear TalEvidencia
 *
 * @author Francisco Alfaro
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Evidencia {
    @Hidden
    private Integer id;

    @Parameter(name = "Img Evidencia", required = true)
    @NotEmpty(message = "La IMG es obligatoria.")
    @ToString.Exclude
    private String img;

    @Parameter(name = "Tipo de Evidencia", required = true)
    @NotEmpty(message = "El Tipo de Evidencia es obligatorio.")
    private String tipoEvidencia;

    @Hidden
    @JsonIgnore
    @ToString.Exclude
    private byte[] byteImg;
}
