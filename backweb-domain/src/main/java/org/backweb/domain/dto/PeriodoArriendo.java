package org.backweb.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Clase DTO para mapear RelPeriodoArriendo
 *
 * @author Francisco Alfaro
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PeriodoArriendo {
    private Date fechaInicio;

    private Date fechaFin;

    private String estado;
}
