package org.backweb.domain.enumeration;

/**
 * Enum EstadoArriendo para definir los Estados de Arriendo
 *
 * @author Francisco Alfaro
 */
public enum EstadoArriendo {
    EN_CURSO,
    TERMINADO,
    PENDIENTE
}
