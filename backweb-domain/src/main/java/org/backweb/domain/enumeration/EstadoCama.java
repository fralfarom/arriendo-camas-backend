package org.backweb.domain.enumeration;

/**
 * Enum EstadoCama para definir los Estados de las Camas
 *
 * @author Francisco Alfaro
 */
public enum EstadoCama {
    DISPONIBLE,
    ARRENDADA
}
