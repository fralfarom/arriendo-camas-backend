package org.backweb.domain.enumeration;

/**
 * Enum EstadoCama para definir los Estados de los Periodos de Arriendo
 *
 * @author Francisco Alfaro
 */
public enum EstadoPeriodoArriendo {
    PAGADO,
    PENDIENTE
}
