package org.backweb.domain.enumeration;

/**
 * Enum EstadoCama para definir los Tipos de Evidencia
 *
 * @author Francisco Alfaro
 */
public enum TipoEvidencia {
    CONTRATO,
    INSTALACION,
    UBICACION
}
