package org.backweb.domain.request;

import lombok.Data;
import org.backweb.domain.dto.Arriendo;

/**
 * Clase que define el Request necesario para Actualizar un Arriendo
 *
 * @author Francisco Alfaro
 */
@Data
public class ActualizarArriendoRequest {
    private Arriendo arriendo;
}
