package org.backweb.domain.request;

import lombok.Data;
import org.backweb.domain.dto.Cama;

/**
 * Clase que define el Request necesario para Actualizar una Cama
 *
 * @author Francisco Alfaro
 */
@Data
public class ActualizarCamaRequest {
    private Cama cama;
}
