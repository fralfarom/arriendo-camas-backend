package org.backweb.domain.request;

import lombok.Data;
import org.backweb.domain.dto.Cliente;

/**
 * Clase que define el Request necesario para Actualizar un Cliente
 *
 * @author Francisco Alfaro
 */
@Data
public class ActualizarClienteRequest {
    private Cliente cliente;
}
