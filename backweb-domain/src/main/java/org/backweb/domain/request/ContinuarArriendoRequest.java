package org.backweb.domain.request;

import io.swagger.v3.oas.annotations.Parameter;
import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Clase que define el Request necesario para Continuar con un Arriendo
 *
 * @author Francisco Alfaro
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ContinuarArriendoRequest {
    @Parameter(name = "ID del Arriendo", required = true)
    @NotEmpty(message = "El ID Arriendo es obligatorio.")
    private Integer idArriendo;

    @Parameter(name = "Fecha Inicio del Arriendo", required = true)
    @NotEmpty(message = "La Fecha Inicio es obligatoria.")
    private Date fechaInicio;

    @Parameter(name = "Fecha fin del Periodo Arriendo", required = true)
    @NotEmpty(message = "La Fecha Fin es obligatoria.")
    private Date fechaFin;


}
