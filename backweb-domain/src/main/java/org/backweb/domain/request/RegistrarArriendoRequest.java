package org.backweb.domain.request;

import io.swagger.v3.oas.annotations.Parameter;
import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.backweb.domain.dto.Evidencia;

import java.util.Date;
import java.util.List;

/**
 * Clase que define el Request necesario para registrar un Cliente.
 *
 * @author Francisco Alfaro
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegistrarArriendoRequest {
    @Parameter(name = "ID del Cliente", required = true)
    @NotEmpty(message = "El ID Cliente es obligatorio.")
    private Integer idCliente;

    @Parameter(name = "ID de Cama", required = true)
    @NotEmpty(message = "El ID Cama es obligatorio.")
    private Integer idCama;

    @Parameter(name = "Monto del Arriendo", required = true)
    @NotEmpty(message = "El Monto es obligatorio.")
    private Integer monto;

    @Parameter(name = "Direccion del Arriendo", required = true)
    @NotEmpty(message = "La Direccion es obligatorio.")
    private String direccion;

    @Parameter(name = "Fecha Inicio del Arriendo", required = true)
    @NotEmpty(message = "La Fecha Inicio es obligatoria.")
    private Date fechaInicio;

    @Parameter(name = "Fecha fin del Periodo Arriendo", required = true)
    @NotEmpty(message = "La Fecha Fin es obligatoria.")
    private Date fechaFin;

    @Parameter(name = "Evidencias", required = true)
    @NotEmpty(message = "Las Evidencias son obligatorias.")
    private List<Evidencia> evidencias;
}
