package org.backweb.domain.request;

import io.swagger.v3.oas.annotations.Parameter;
import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Clase que define el Request necesario para registrar una Cama
 *
 * @author Francisco Alfaro
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RegistrarCamaRequest {
    @Parameter(name = "Codigo cama", required = true)
    @NotEmpty(message = "Codigo Cama es obligatorio")
    private String codigo;
}
