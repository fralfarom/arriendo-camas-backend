package org.backweb.domain.request;

import io.swagger.v3.oas.annotations.Parameter;
import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Clase que define el Request necesario para registrar un Cliente.
 *
 * @author Francisco Alfaro
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegistrarClienteRequest {
    @Parameter(name = "RUT del Cliente", required = true)
    @NotEmpty(message = "RUT Cliente es obligatorio")
    private String rut;

    @Parameter(name = "Nombre del Cliente", required = true)
    @NotEmpty(message = "Nombre Cliente es obligatorio")
    private String nombre;

    @Parameter(name = "Telefono del Cliente", required = true)
    @NotEmpty(message = "Telefono Cliente es obligatorio")
    private String telefono;
}
