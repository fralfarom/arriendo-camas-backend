package org.backweb.domain.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.backweb.domain.dto.Arriendo;
import org.backweb.domain.dto.Evidencia;
import org.backweb.domain.dto.PeriodoArriendo;

import java.util.List;

/**
 * Clase que define el Response para ObtenerArriendoById
 *
 * @author Francisco Alfaro
 */
@Data
@AllArgsConstructor
public class ObtenerArriendoByIdResponse {
    private Arriendo arriendo;

    private List<Evidencia> evidencias;

    private List<PeriodoArriendo> periodos;
}
