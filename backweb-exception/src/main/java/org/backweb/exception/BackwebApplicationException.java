package org.backweb.exception;

/**
 * Clase para manejar BackwebApplicationException
 *
 * @author Francisco Alfaro
 */
public class BackwebApplicationException extends BackwebException {

	private static final long serialVersionUID = 1L;

	
	public BackwebApplicationException(BackwebExceptionCodes code) {
		super(code);
	}
	public BackwebApplicationException(BackwebExceptionCodes code, String... params) {
		super(code, params);
	}

	public BackwebApplicationException(String message, BackwebExceptionCodes code, String... params) {
		super(message, code, params);
	}

	public BackwebApplicationException(String message, Throwable cause, BackwebExceptionCodes code, String... params) {
		super(message, cause, code, params);
	}

	public BackwebApplicationException(String message, Throwable cause, boolean enableSuppression,
									   boolean writableStackTrace, BackwebExceptionCodes code, String... params) {
		super(message, cause, enableSuppression, writableStackTrace, code, params);
	}

	public BackwebApplicationException(Throwable cause, BackwebExceptionCodes code, String... params) {
		super(cause, code, params);
	}
}
