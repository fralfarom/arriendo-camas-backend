package org.backweb.exception;

/**
 * Clase para manejar BackwebBusinessException
 *
 * @author Francisco Alfaro
 */
public class BackwebBusinessException extends BackwebException {

	private static final long serialVersionUID = 1L;

	public BackwebBusinessException(BackwebExceptionCodes code) {
		super(code);
	}

	public BackwebBusinessException(BackwebExceptionCodes code, String... params) {
		super(code, params);
	}
	
	public BackwebBusinessException(String message, BackwebExceptionCodes code, String... params) {
		super(message, code, params);
	}

	public BackwebBusinessException(String message, Throwable cause, BackwebExceptionCodes code, String... params) {
		super(message, cause, code, params);
	}

	public BackwebBusinessException(String message, Throwable cause, boolean enableSuppression,
									boolean writableStackTrace, BackwebExceptionCodes code, String... params) {
		super(message, cause, enableSuppression, writableStackTrace, code, params);
	}

	public BackwebBusinessException(Throwable cause, BackwebExceptionCodes code, String... params) {
		super(cause, code, params);
	}
	
}
