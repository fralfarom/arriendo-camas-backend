package org.backweb.exception;

import lombok.Getter;
import org.backweb.common.constant.Constantes;

/**
 * Clase abstracta para manejar Excepciones
 *
 * @author Francisco Alfaro
 */
@Getter
public abstract class BackwebException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private final BackwebExceptionCodes code;
	private final String[] params;

	protected BackwebException(BackwebExceptionCodes code) {
		super(code.getMessage());
		this.code = code;
		this.params = new String[] {};
	}
	
	protected BackwebException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace,
							   BackwebExceptionCodes code, String... params) {
		super(message, cause, enableSuppression, writableStackTrace);
		this.code = code;
		this.params = params;
	}
	
	protected BackwebException(String message, Throwable cause, BackwebExceptionCodes code, String... params) {
		super(message, cause);
		this.code = code;
		this.params = params;
	}
	
	protected BackwebException(BackwebExceptionCodes code, String... params) {
		this.code = code;
		this.params = params;
	}

	protected BackwebException(String message, BackwebExceptionCodes code, String... params) {
		super(message);
		this.code = code;
		this.params = params;
	}

	protected BackwebException(Throwable cause, BackwebExceptionCodes code, String... params) {
		super(cause);
		this.code = code;
		this.params = params;
	}
	@Override
	public String toString() {

		String msg = code.toString();

		int index = 0;
		for (String param : params) {
			msg = msg.replace(Constantes.LEFT_CURLY_BRACKET + index++ + Constantes.RIGHT_CURLY_BRACKET, param);
		}

		return msg;

	}

	@Override
	public String getMessage() {
		return code.name() + Constantes.COLON + this.toString();
	}
}
