package org.backweb.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Enum de exception codes de la aplicacion.
 *
 * @author FranciscoAlfaro
 */
@Getter
@AllArgsConstructor
public enum BackwebExceptionCodes {

	// Errores de Excepcion de Aplicacion
	DEMO_MODULO1_APP_0000("Soy un error de aplicación"),
	
	DEMO_SEGURIDAD_APP_0001("Error generando Token"),

	// Errores de Excepcion de Negocio
	DEMO_MODULO1_BIZ_0000("Soy un error de negocio"), 
	
	//Errores API Cama
	BACKWEB_CAMA_0001("Error al guardar Cama."),
	BACKWEB_CAMA_0002("Error al encontrar las Camas."),
	BACKWEB_CAMA_0003("Error al encontrar la Cama."),

	BACKWEB_CLIENTE_0001("Error al guardar el Cliente."),
	BACKWEB_CLIENTE_0002("Error al encontrar los Clientes."),
	BACKWEB_CLIENTE_0003("Error al encontrar el Cliente."),
	BACKWEB_CLIENTE_0004("Error al guardar Arriendo."),

	BACKWEB_ARRIENDO_0001("Error al guardar el Arriendo."),
	BACKWEB_ARRIENDO_0002("Error al encontrar los Arriendos."),
	BACKWEB_ARRIENDO_0003("Error al encontrar el Arriendo."),
	BACKWEB_ARRIENDO_0004("Error al eliminar el Arriendo."),

	BACKWEB_GLOB_SEC_0001("Acceso Denegado"),
	
	BACKWEB_SEGURIDAD_BIZ_0001("La cabecera de autenticación no es valida"),
	BACKWEB_SEGURIDAD_BIZ_0002("El token, no es un token valido"),
	BACKWEB_SEGURIDAD_BIZ_0003("El usuario no existe"),
	BACKWEB_SEGURIDAD_BIZ_0004("El Token ha expirado o es invalido"),
	BACKWEB_SEGURIDAD_BIZ_0005("No se pudo obtener el Token"),
	BACKWEB_SEGURIDAD_BIZ_0006("Credenciales Invalidas"),
	BACKWEB_SEGURIDAD_BIZ_0007("Usuario Deshabilitado"),
	BACKWEB_SEGURIDAD_BIZ_0008("No se pudo parsear el token"),
	BACKWEB_SEGURIDAD_BIZ_0009("Token con firma invalida"),
	BACKWEB_SEGURIDAD_BIZ_0010("El rol es invalido"),
	
	// Errores de Excepcion Genericos
	BACKWEB_GLOB_BIZ_0001("URL mal escrita, sintaxis malformada, o una URL contiene caracteres ilegales"),
	BACKWEB_GLOB_BIZ_0002("Uno de los ids informados no se encuentran registrados en base de datos lo que genera un error de integridad de datos."),
	BACKWEB_GLOB_APP_0001("Ocurrio un error inesperado"),
	BACKWEB_GLOB_BIZ_0003("Violación de restricción en los datos"),
	BACKWEB_GLOB_BIZ_0004("Violación de integridad de los datos");

	private final String message;

	@Override
	public String toString() {
		return this.message;
	}

}
