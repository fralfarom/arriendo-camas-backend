package org.backweb.persistence.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Clase que define la tabla rel_arriendo_evidencia para persistencia.
 *
 * @author Francisco Alfaro
 */
@Entity
@Table(name = "rel_arriendo_evidencia")

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RelArriendoEvidencia {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "id_arriendo")
    private TalArriendo arriendo;

    @ManyToOne
    @JoinColumn(name = "id_evidencia")
    private TalEvidencia evidencia;
}
