package org.backweb.persistence.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Clase que define la tabla rel_periodo_arriendo para persistencia.
 *
 * @author Francisco Alfaro
 */
@Entity
@Table(name = "rel_periodo_arriendo")

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RelPeriodoArriendo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "fecha_inicio")
    private Date fechaInicio;

    @Column(name = "fecha_fin")
    private Date fechaFin;

    @Column(name = "estado")
    private String estado;

    @ManyToOne
    @JoinColumn(name = "id_arriendo")
    private TalArriendo arriendo;
}
