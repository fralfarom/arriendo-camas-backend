package org.backweb.persistence.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

/**
 * Clase que define la tabla tal_arriendo para persistencia.
 *
 * @author Francisco Alfaro
 */
@Entity
@Table(name = "tal_arriendo")

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TalArriendo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "monto", nullable = false)
    private Integer monto;

    @Column(name = "estado", nullable = false)
    private String estado;

    @Column(name = "direccion", nullable = false)
    private String direccion;

    @ManyToOne
    @JoinColumn(name = "id_cliente", nullable = false)
    private TalCliente cliente;

    @ManyToOne
    @JoinColumn(name = "id_cama", nullable = false)
    private TalCama cama;

    @ToString.Exclude
    @OneToMany(mappedBy = "arriendo", cascade = CascadeType.ALL)
    private List<RelArriendoEvidencia> relArriendoEvidencias;

    @ToString.Exclude
    @OneToMany(mappedBy = "arriendo", cascade = CascadeType.ALL)
    private List<RelPeriodoArriendo> periodos;
}
