package org.backweb.persistence.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Clase que define la tabla tal_cama para persistencia.
 *
 * @author Francisco Alfaro
 */
@Entity
@Table(name = "tal_cama")

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TalCama {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "codigo", nullable = false)
    private String codigo;

    @Column(name = "estado", nullable = false)
    private String estado;
}
