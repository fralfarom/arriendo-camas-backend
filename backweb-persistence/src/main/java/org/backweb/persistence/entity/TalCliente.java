package org.backweb.persistence.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Clase que define la tabla tal_cliente para persistencia.
 *
 * @author Francisco Alfaro
 */
@Entity
@Table(name = "tal_cliente")

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TalCliente {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "rut", nullable = false)
    private String rut;

    @Column(name = "nombre", nullable = false)
    private String nombre;

    @Column(name = "telefono", nullable = false)
    private String telefono;

    // Constructor, getters, setters, etc.
}
