package org.backweb.persistence.entity;


import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

/**
 * Clase que define la tabla tal_evidencias para persistencia.
 *
 * @author Francisco Alfaro
 */
@Entity
@Table(name = "tal_evidencias")

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TalEvidencia {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @ToString.Exclude
    @Column(name = "imagen", columnDefinition = "bytea", nullable = false)
    private byte[] imagen;

    @Column(name = "tipo_evidencia", nullable = false)
    private String tipoEvidencia;

    @ToString.Exclude
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "evidencia")
    private List<RelArriendoEvidencia> relArriendoEvidencias;
}
