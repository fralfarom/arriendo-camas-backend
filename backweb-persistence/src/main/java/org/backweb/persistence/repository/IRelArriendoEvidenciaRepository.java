package org.backweb.persistence.repository;

import org.backweb.persistence.entity.RelArriendoEvidencia;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Clase que define el repository de la entidad RelArriendoEvidencia.
 *
 * @author Francisco Alfaro
 */
public interface IRelArriendoEvidenciaRepository extends JpaRepository<RelArriendoEvidencia, Integer> {
}
