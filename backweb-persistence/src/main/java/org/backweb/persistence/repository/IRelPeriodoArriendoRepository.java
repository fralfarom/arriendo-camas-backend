package org.backweb.persistence.repository;

import org.backweb.persistence.entity.RelPeriodoArriendo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Clase que define el repository de la entidad RelPeriodoArriendo.
 *
 * @author Francisco Alfaro
 */
public interface IRelPeriodoArriendoRepository extends JpaRepository<RelPeriodoArriendo, Integer> {
    @Query("SELECT rpa FROM RelPeriodoArriendo rpa WHERE rpa.arriendo.id = :idArriendo AND rpa.estado = :estado")
    RelPeriodoArriendo findByIdArriendoAndEstado(@Param("idArriendo") Integer idArriendo, @Param("estado") String estado);

    @Query("SELECT rpa FROM RelPeriodoArriendo rpa WHERE rpa.arriendo.id = :idArriendo")
    List<RelPeriodoArriendo> findByIdArriendo(@Param("idArriendo") Integer idArriendo);
}
