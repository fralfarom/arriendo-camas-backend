package org.backweb.persistence.repository;

import org.backweb.persistence.entity.TalArriendo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Clase que define el repository de la entidad TalArriendo.
 *
 * @author Francisco Alfaro
 */
@Repository
public interface ITalArriendoRepository extends JpaRepository<TalArriendo, Integer> {
}
