package org.backweb.persistence.repository;

import org.backweb.persistence.entity.TalCama;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Clase que define el repository de la entidad TalCama.
 *
 * @author Francisco Alfaro
 */
@Repository
public interface ITalCamaRepository extends JpaRepository<TalCama, Integer> {
}
