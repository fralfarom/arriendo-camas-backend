package org.backweb.persistence.repository;

import org.backweb.persistence.entity.TalCliente;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Clase que define el repository de la entidad TalCliente.
 *
 * @author Francisco Alfaro
 */
public interface ITalClienteRepository extends JpaRepository<TalCliente, Integer> {
}
