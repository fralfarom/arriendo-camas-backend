package org.backweb.persistence.repository;

import org.backweb.persistence.entity.TalEvidencia;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Clase que define el repository de la entidad TalEvidencia.
 *
 * @author Francisco Alfaro
 */
public interface ITalEvidenciasRepository extends JpaRepository<TalEvidencia, Integer> {
    @Query("SELECT te FROM TalEvidencia te INNER JOIN te.relArriendoEvidencias rae WHERE rae.arriendo.id = :idArriendo")
    List<TalEvidencia> obtenerEvidenciasByIdArriendo(@Param("idArriendo") Integer idArriendo);
}
