package org.backweb.service;

import org.backweb.common.messaging.CreatedResponse;
import org.backweb.common.messaging.GetResponse;
import org.backweb.domain.dto.Arriendo;
import org.backweb.domain.request.ActualizarArriendoRequest;
import org.backweb.domain.request.ContinuarArriendoRequest;
import org.backweb.domain.request.RegistrarArriendoRequest;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Interfaz que contiene los metodos para Arriendos.
 *
 * @author FranciscoAlfaro
 *
 */
public interface IArriendoService {
    /**
     * Registra una nuevo Arriendo.
     * @param request {@link RegistrarArriendoRequest}
     * @return {@link CreatedResponse}
     */
    public CreatedResponse registrarArriendo(RegistrarArriendoRequest request);

    /**
     * Termina un Arriendo.
     * @param idArriendo ID del Arriendo a terminar.
     * @return {@link CreatedResponse}
     */
    @Transactional
    CreatedResponse terminarArriendo(Integer idArriendo);

    /**
     * Continua un Arriendo.
     * @param request {@link ContinuarArriendoRequest}
     * @return {@link CreatedResponse}
     */
    CreatedResponse continuarArriendo(ContinuarArriendoRequest request);

    /**
     * Registra una nuevo Arriendo.
     * @param idArriendo ID del Arriendo a pagar.
     * @return {@link CreatedResponse}
     */
    CreatedResponse pagarPeriodoArriendo(Integer idArriendo);

    /**
     * Obtiene todos los Arriendo.
     * @return {@link List<Arriendo>}
     */
    public GetResponse obtenerArriendos();

    /**
     * Obtiene un Arriendo por Id.
     * @param id ID del Arriendo a obtener.
     * @return {@link Arriendo}
     */
    public GetResponse obtenerArriendoById(Integer id);

    /**
     * Actualiza un Arriendo por Id.
     * @param request {@link ActualizarArriendoRequest}
     * @return {@link Arriendo}
     */
    public GetResponse actualizarArriendoById(ActualizarArriendoRequest request);

    /**
     * Elimina un Arriendo por Id.
     * @param id ID del Arriendo a eliminar.
     * @return {@link Arriendo}
     */
    public GetResponse eliminarArriendoById(Integer id);

    /**
     * Expone los tipos de Evidencias para un Arriendo.
     *
     * @return {@link GetResponse}
     */
    public GetResponse obtenerTiposEvidencia();
}
