package org.backweb.service;

import org.backweb.common.messaging.CreatedResponse;
import org.backweb.common.messaging.GetResponse;
import org.backweb.domain.dto.Cama;
import org.backweb.domain.request.ActualizarCamaRequest;
import org.backweb.domain.request.RegistrarCamaRequest;

import java.util.List;

/**
 * Interfaz que contiene los metodos para Camas.
 *
 * @author FranciscoAlfaro
 *
 */
public interface ICamaService {
    /**
     * Registra una nueva cama.
     * @param request {@link RegistrarCamaRequest}
     * @return {@link CreatedResponse}
     */
    public CreatedResponse registrarCama(RegistrarCamaRequest request);

    /**
     * Obtiene todas las Cama.
     * @return {@link List<Cama>}
     */
    public GetResponse obtenerCamas();

    /**
     * Obtiene una Cama por ID.
     * @param id ID de la cama a obtener.
     * @return {@link Cama}
     */
    public GetResponse obtenerCamaById(Integer id);

    /**
     * Actualiza una Cama por ID.
     * @param request {@link ActualizarCamaRequest}
     * @return {@link Cama}
     */
    public GetResponse actualizarCamaById(ActualizarCamaRequest request);

    /**
     * Elimina una Cama por ID.
     * @param id ID de la Cama a eliminar.
     * @return {@link Cama}
     */
    public GetResponse eliminarCamaById(Integer id);
}
