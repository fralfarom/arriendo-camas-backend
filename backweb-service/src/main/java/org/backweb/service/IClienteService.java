package org.backweb.service;

import org.backweb.common.messaging.CreatedResponse;
import org.backweb.common.messaging.GetResponse;
import org.backweb.domain.dto.Cliente;
import org.backweb.domain.request.ActualizarClienteRequest;
import org.backweb.domain.request.RegistrarClienteRequest;

import java.util.List;

/**
 * Interfaz que contiene los metodos para Clientes.
 *
 * @author FranciscoAlfaro
 *
 */
public interface IClienteService {
    /**
     * Registra un nuevo Cliente.
     * @param request {@link RegistrarClienteRequest}
     * @return {@link CreatedResponse}
     */
    public CreatedResponse registrarCliente(RegistrarClienteRequest request);

    /**
     * Obtiene todos los Clientes.
     * @return {@link List<Cliente>}
     */
    public GetResponse obtenerClientes();

    /**
     * Obtiene un Cliente por ID.
     * @param id ID del Cliente a obtener.
     * @return {@link Cliente}
     */
    public GetResponse obtenerClienteById(Integer id);

    /**
     * Actualiza un Cliente por ID.
     * @param request {@link ActualizarClienteRequest}
     * @return {@link Cliente}
     */
    public GetResponse actualizarClienteById(ActualizarClienteRequest request);

    /**
     * Elimina un Cliente por ID.
     * @param id ID del Cliente a eliminar.
     * @return {@link Cliente}
     */
    public GetResponse eliminarClienteById(Integer id);
}
