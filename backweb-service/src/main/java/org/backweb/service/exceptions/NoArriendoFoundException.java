package org.backweb.service.exceptions;

/**
 * Clase de exception NoArriendoFoundException.
 *
 * @author Francisco Alfaro
 */
public class NoArriendoFoundException extends Exception{
    private static final long serialVersionUID = 8472481258731124698L;

}
