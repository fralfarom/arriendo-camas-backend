package org.backweb.service.exceptions;

/**
 * Clase de exception NoCamasFoundException.
 *
 * @author Francisco Alfaro
 */
public class NoCamasFoundException extends Exception {
	private static final long serialVersionUID = 8472481258731124698L;

}
