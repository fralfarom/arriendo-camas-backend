package org.backweb.service.exceptions;

/**
 * Clase de exception NoClientesFoundException.
 *
 * @author Francisco Alfaro
 */
public class NoClientesFoundException extends Exception {
	private static final long serialVersionUID = 8472481258731124698L;

}
