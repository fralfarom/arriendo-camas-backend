package org.backweb.service.exceptions;

/**
 * Clase de exception NoPeriodoArriendoFoundException.
 *
 * @author Francisco Alfaro
 */
public class NoPeriodoArriendoFoundException extends Exception{
    private static final long serialVersionUID = 8472481258731124698L;

}
