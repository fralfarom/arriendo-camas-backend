package org.backweb.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.backweb.common.messaging.CreatedResponse;
import org.backweb.common.messaging.GetResponse;
import org.backweb.domain.dto.*;
import org.backweb.domain.enumeration.EstadoArriendo;
import org.backweb.domain.enumeration.EstadoCama;
import org.backweb.domain.enumeration.EstadoPeriodoArriendo;
import org.backweb.domain.enumeration.TipoEvidencia;
import org.backweb.domain.request.ActualizarArriendoRequest;
import org.backweb.domain.request.ContinuarArriendoRequest;
import org.backweb.domain.request.RegistrarArriendoRequest;
import org.backweb.domain.response.ObtenerArriendoByIdResponse;
import org.backweb.exception.BackwebBusinessException;
import org.backweb.exception.BackwebExceptionCodes;
import org.backweb.persistence.entity.*;
import org.backweb.persistence.repository.*;
import org.backweb.service.IArriendoService;
import org.backweb.service.exceptions.NoArriendoFoundException;
import org.backweb.service.exceptions.NoCamasFoundException;
import org.backweb.service.exceptions.NoPeriodoArriendoFoundException;
import org.backweb.service.util.Utils;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

/**
 * Implementacion de servicios ArriendoService
 *
 * @author FranciscoAlfaro
 */
@Slf4j
@Service
public class ArriendoServiceImpl implements IArriendoService {
    @Autowired
    ITalArriendoRepository arriendoRepository;
    @Autowired
    ITalEvidenciasRepository evidenciasRepository;
    @Autowired
    IRelArriendoEvidenciaRepository arriendoEvidenciaRepository;
    @Autowired
    ITalCamaRepository camaRepository;
    @Autowired
    IRelPeriodoArriendoRepository periodoArriendoRepository;
    @Autowired
    ModelMapper arriendoMapper;

    /**{@inheritDoc}*/
    @Override
    @Transactional
    public CreatedResponse registrarArriendo(RegistrarArriendoRequest request) {
        try {
            log.info("[ArriendoServiceImpl][registrarArriendo][INICIO] Request: " + request);

            //Crear Cliente del Arriendo
            Cliente clienteNuevo = new Cliente(request.getIdCliente(), null, null, null);
            log.info("[ArriendoServiceImpl][registrarArriendo][INICIO] Cliente: " + clienteNuevo);

            //Crear Cama del Arriendo
            Cama cama = new Cama(request.getIdCama(), null, EstadoCama.ARRENDADA.toString());
            log.info("[ArriendoServiceImpl][registrarArriendo] Cama: " + cama);

            //Crear Arriendo
            Arriendo arriendo = new Arriendo(null, clienteNuevo, cama, request.getMonto(), request.getDireccion());
            log.info("[ArriendoServiceImpl][registrarArriendo] Arriendo: " + arriendo);

            //Crear Evidencia
            List<Evidencia> evidenciasRequest = request.getEvidencias().stream().map(evidencia -> {
                evidencia.setByteImg(Base64.getDecoder().decode(evidencia.getImg()));
                return evidencia;
            }).toList();

            //Guardar Evidencias
            List<TalEvidencia> listaEvidencias = arriendoMapper.map(evidenciasRequest, new TypeToken<List<TalEvidencia>>() {}.getType());
            log.info("[ArriendoServiceImpl][registrarArriendo] ListaEvidencias: " + listaEvidencias);
            List<TalEvidencia> evidenciasGuardadas = evidenciasRepository.saveAll(listaEvidencias);
            log.info("[ArriendoServiceImpl][registrarArriendo] EvidenciasGuardadas: " + evidenciasGuardadas);

            //Guardar Arriendo
            TalArriendo arriendoEntity = arriendoMapper.map(arriendo, TalArriendo.class);
            arriendoEntity.setEstado(EstadoArriendo.EN_CURSO.toString());
            log.info("[ArriendoServiceImpl][registrarArriendo] ArriendoEntity: " + arriendoEntity);
            arriendoEntity = arriendoRepository.save(arriendoEntity);
            log.info("[ArriendoServiceImpl][registrarArriendo] Save ArriendoEntity: " + arriendoEntity);

            //Guardar Rel Arriendo Evidencias
            TalArriendo finalArriendoEntity = arriendoEntity;
            List<RelArriendoEvidencia> listArriendoEvidencia = evidenciasGuardadas.stream()
                    .map(evidencia -> new RelArriendoEvidencia(null, finalArriendoEntity, evidencia)).toList();

            List<RelArriendoEvidencia> arriendoEvidenciaEntity = arriendoEvidenciaRepository.saveAll(listArriendoEvidencia);
            log.info("[ArriendoServiceImpl][registrarArriendo] ArriendoEvidenciaEntity: " + arriendoEvidenciaEntity);

            //Obtener cama arrendada
            TalCama camaArrendada = camaRepository.findById(request.getIdCama()).orElseThrow(NoCamasFoundException::new);

            //Actualizar estado cama a ARRENDADA
            camaArrendada.setEstado(EstadoCama.ARRENDADA.toString());
            camaRepository.save(camaArrendada);

            //Crear periodo arriendo
            periodoArriendoRepository.save(new RelPeriodoArriendo(null, request.getFechaInicio(), request.getFechaFin(), EstadoPeriodoArriendo.PENDIENTE.toString(), arriendoEntity));

            return new CreatedResponse(arriendoEntity.getId(), Utils.nowStringLDTAmericaSantiago());
        } catch (NoCamasFoundException e) {
            log.info("[ArriendoServiceImpl][registrarArriendo] NoCamasFoundException: " + e);
            throw new BackwebBusinessException(BackwebExceptionCodes.BACKWEB_CAMA_0003);
        } catch (DataIntegrityViolationException e) {
            log.info("[ArriendoServiceImpl][registrarArriendo] Error: " + e);
            throw new BackwebBusinessException(BackwebExceptionCodes.BACKWEB_CLIENTE_0004);
        }
    }

    /**{@inheritDoc}*/
    @Override
    @Transactional
    public CreatedResponse terminarArriendo(Integer idArriendo) {
        try {
            log.info("[ArriendoServiceImpl][terminarArriendo] Request: " + idArriendo);

            //Obtener el Arriendo
            TalArriendo arriendoEntity = arriendoRepository.findById(idArriendo).orElseThrow(NoArriendoFoundException::new);

            //Terminar el Arriendo
            arriendoEntity.setEstado(EstadoArriendo.TERMINADO.toString());
            arriendoEntity = arriendoRepository.save(arriendoEntity);

            //Obtener Cama del Arriendo
            TalCama camaArrendada = camaRepository.findById(arriendoEntity.getCama().getId()).orElseThrow(NoCamasFoundException::new);

            //Actualizar estado cama a DISPONIBLE
            camaArrendada.setEstado(EstadoCama.DISPONIBLE.toString());
            camaRepository.save(camaArrendada);

            return new CreatedResponse(arriendoEntity.getId(), Utils.nowStringLDTAmericaSantiago());
        } catch (NoCamasFoundException e) {
            log.info("[ArriendoServiceImpl][terminarArriendo] NoCamasFoundException: " + e);
            throw new BackwebBusinessException(BackwebExceptionCodes.BACKWEB_ARRIENDO_0003);
        } catch (NoArriendoFoundException e) {
            log.info("[ArriendoServiceImpl][terminarArriendo] NoArriendoFoundException: " + e);
            throw new BackwebBusinessException(BackwebExceptionCodes.BACKWEB_ARRIENDO_0003);
        } catch (DataIntegrityViolationException e) {
            log.info("[ArriendoServiceImpl][terminarArriendo] DataIntegrityViolationException: " + e);
            throw new BackwebBusinessException(BackwebExceptionCodes.BACKWEB_CLIENTE_0004);
        }
    }

    /**{@inheritDoc}*/
    @Override
    public CreatedResponse continuarArriendo(ContinuarArriendoRequest request) {
        log.info("[ArriendoServiceImpl][continuarArriendo] Request: " + request);

        try {
            //Obtener el Arriendo
            TalArriendo arriendoEntity = arriendoRepository.findById(request.getIdArriendo()).orElseThrow(NoArriendoFoundException::new);
            log.info("[ArriendoServiceImpl][continuarArriendo] ArriendoEntity: " + arriendoEntity);

            //Crear nuevo periodo
            RelPeriodoArriendo periodoArriendo = periodoArriendoRepository.save(new RelPeriodoArriendo(null, request.getFechaInicio(), request.getFechaFin(), EstadoPeriodoArriendo.PENDIENTE.toString(), arriendoEntity));
            log.info("[ArriendoServiceImpl][continuarArriendo] PeriodoArriendo: " + periodoArriendo);

            return new CreatedResponse(periodoArriendo.getId(), Utils.nowStringLDTAmericaSantiago());
        } catch (NoArriendoFoundException e) {
            log.info("[ArriendoServiceImpl][continuarArriendo] NoArriendoFoundException: " + e);
            throw new BackwebBusinessException(BackwebExceptionCodes.BACKWEB_ARRIENDO_0003);
        } catch (DataIntegrityViolationException e) {
            log.info("[ArriendoServiceImpl][continuarArriendo] DataIntegrityViolationException: " + e);
            throw new BackwebBusinessException(BackwebExceptionCodes.BACKWEB_CLIENTE_0004);
        }
    }

    /**{@inheritDoc}*/
    @Override
    public CreatedResponse pagarPeriodoArriendo(Integer idArriendo) {
        log.info("[ArriendoServiceImpl][continuarArriendo] IdArriendo: " + idArriendo);

        try {
            //Obtener el Periodo Arriendo
            RelPeriodoArriendo periodoArriendo = periodoArriendoRepository.findByIdArriendoAndEstado(idArriendo, EstadoPeriodoArriendo.PENDIENTE.toString());
            log.info("[ArriendoServiceImpl][continuarArriendo] PeriodoArriendo: " + periodoArriendo);
            if(periodoArriendo == null) throw new NoPeriodoArriendoFoundException();

            //Cambiar estado a Pagado
            periodoArriendo.setEstado(EstadoPeriodoArriendo.PAGADO.toString());
            periodoArriendo = periodoArriendoRepository.save(periodoArriendo);

            return new CreatedResponse(periodoArriendo.getId(), Utils.nowStringLDTAmericaSantiago());
        } catch (NoPeriodoArriendoFoundException e) {
            log.info("[ArriendoServiceImpl][continuarArriendo] NoArriendoFoundException: " + e);
            throw new BackwebBusinessException(BackwebExceptionCodes.BACKWEB_ARRIENDO_0003);
        } catch (DataIntegrityViolationException e) {
            log.info("[ArriendoServiceImpl][continuarArriendo] DataIntegrityViolationException: " + e);
            throw new BackwebBusinessException(BackwebExceptionCodes.BACKWEB_CLIENTE_0004);
        }
    }

    /**{@inheritDoc}*/
    @Override
    public GetResponse obtenerArriendos() {
        try {
            log.info("[ArriendoServiceImpl][obtenerArriendos][INICIO]");
            List<TalArriendo> arriendos = arriendoRepository.findAll();
            log.info("[ArriendoServiceImpl][obtenerArriendos] Arriendos: " + arriendos);

            Type listTypeArriendos = new TypeToken<List<Arriendo>>() {}.getType();
            List<Arriendo> listaArriendos = arriendoMapper.map(arriendos, listTypeArriendos);
            log.info("[ArriendoServiceImpl][obtenerArriendos][FIN] Return: " + listaArriendos);
            return new GetResponse(listaArriendos);
        }catch (Exception e) {
            log.info("[ArriendoServiceImpl][obtenerArriendos] Error: " + e);
            throw new BackwebBusinessException(BackwebExceptionCodes.BACKWEB_ARRIENDO_0002);
        }
    }

    /**{@inheritDoc}*/
    @Override
    public GetResponse obtenerArriendoById(Integer id) {
        try {
            log.info("[ArriendoServiceImpl][obtenerArriendoById] ID: " + id);
            //Obtener Arriendo o lanzar excepcion si no encontramos
            TalArriendo arriendoEntity = arriendoRepository.findById(id).orElseThrow(NoCamasFoundException::new);
            log.info("[ArriendoServiceImpl][obtenerArriendoById] ArriendoEntity: " + arriendoEntity);

            //Obtener Evidencias del Arriendo
            List<TalEvidencia> evidenciasEntity = evidenciasRepository.obtenerEvidenciasByIdArriendo(id);
            log.info("[ArriendoServiceImpl][obtenerArriendoById] EvidenciasEntity: " + evidenciasEntity);

            //Mappear TalArriendo a Arriendo
            Arriendo arriendo = arriendoMapper.map(arriendoEntity, Arriendo.class);
            log.info("[ArriendoServiceImpl][obtenerArriendoById] Arriendo: " + arriendo);

            //Mappear List<TalEvidencia> a Evidencia
            List<Evidencia> listaEvidencias = evidenciasEntity.stream().map(val -> new Evidencia(
                    val.getId(),
                    Base64.getEncoder().encodeToString(val.getImagen()),
                    val.getTipoEvidencia(),
                    null)
            ).toList();
            log.info("[ArriendoServiceImpl][obtenerArriendoById] ListaEvidencias: " + listaEvidencias);

            //Obtener Periodos de Arriendo
            List<RelPeriodoArriendo> listaPeriodoArriendosEntity = periodoArriendoRepository.findByIdArriendo(id);
            log.info("[ArriendoServiceImpl][obtenerArriendoById] ListaPeriodoArriendosEntity: " + listaPeriodoArriendosEntity);
            List<PeriodoArriendo> listaPeriodoArriendos = arriendoMapper.map(listaPeriodoArriendosEntity, new TypeToken<List<PeriodoArriendo>>() {}.getType());
            log.info("[ArriendoServiceImpl][obtenerArriendoById] ListaPeriodoArriendos: " + listaPeriodoArriendos);

            //Armar response ObtenerArriendoById
            ObtenerArriendoByIdResponse response = new ObtenerArriendoByIdResponse(arriendo, listaEvidencias, listaPeriodoArriendos);
            log.info("[ArriendoServiceImpl][obtenerArriendoById][FIN] Response: " + response);

            return new GetResponse(response);
        }catch (NoCamasFoundException e) {
            log.info("[ArriendoServiceImpl][obtenerArriendoById] Error: " + e);
            throw new BackwebBusinessException(BackwebExceptionCodes.BACKWEB_ARRIENDO_0003);
        }
    }

    /**{@inheritDoc}*/
    @Override
    public GetResponse actualizarArriendoById(ActualizarArriendoRequest request) {
        try {
            log.info("[ArriendoServiceImpl][actualizarArriendoById] Request: " + request);
            //Obtener Arriendo o lanzar excepcion si no encontramos
            TalArriendo arriendoEntity = arriendoRepository.findById(request.getArriendo().getId()).orElseThrow(NoArriendoFoundException::new);
            log.info("[ArriendoServiceImpl][actualizarArriendoById] ArriendoEntity: " + arriendoEntity);

            //Actualizar Arriendo
            arriendoEntity = arriendoRepository.save(arriendoMapper.map(request.getArriendo(), TalArriendo.class));

            return new GetResponse(arriendoEntity);
        }catch (NoArriendoFoundException e) {
            log.info("[ArriendoServiceImpl][actualizarArriendoById] Error NoArriendoFoundException: " + e);
            throw new BackwebBusinessException(BackwebExceptionCodes.BACKWEB_ARRIENDO_0003);
        } catch (DataIntegrityViolationException e) {
            log.info("[ArriendoServiceImpl][actualizarArriendoById] Error DataIntegrityViolationException: " + e);
            throw new BackwebBusinessException(BackwebExceptionCodes.BACKWEB_ARRIENDO_0001);
        }
    }

    /**{@inheritDoc}*/
    @Override
    public GetResponse eliminarArriendoById(Integer id) {
        try{
            arriendoRepository.deleteById(id);
            return new GetResponse(id);
        } catch (Exception e) {
            log.info("[ArriendoServiceImpl][actualizarArriendoById] Error: " + e);
            throw new BackwebBusinessException(BackwebExceptionCodes.BACKWEB_ARRIENDO_0004);
        }
    }

    /**{@inheritDoc}*/
    @Override
    public GetResponse obtenerTiposEvidencia() {
        try{
            return new GetResponse(TipoEvidencia.values());
        } catch (Exception e) {
            log.info("[ArriendoServiceImpl][obtenerTiposEvidencia] Error: " + e);
            throw new BackwebBusinessException(BackwebExceptionCodes.BACKWEB_ARRIENDO_0004);
        }
    }
}
