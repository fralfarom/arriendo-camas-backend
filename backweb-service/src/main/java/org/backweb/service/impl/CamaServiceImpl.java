package org.backweb.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.backweb.common.messaging.CreatedResponse;
import org.backweb.common.messaging.GetResponse;
import org.backweb.domain.dto.Cama;
import org.backweb.domain.enumeration.EstadoCama;
import org.backweb.domain.request.ActualizarCamaRequest;
import org.backweb.domain.request.RegistrarCamaRequest;
import org.backweb.exception.BackwebBusinessException;
import org.backweb.exception.BackwebExceptionCodes;
import org.backweb.persistence.entity.TalCama;
import org.backweb.persistence.repository.ITalCamaRepository;
import org.backweb.service.ICamaService;
import org.backweb.service.exceptions.NoCamasFoundException;
import org.backweb.service.util.Utils;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Implementacion de servicios CamaService
 *
 * @author FranciscoAlfaro
 */
@Slf4j
@Service
public class CamaServiceImpl implements ICamaService {
    @Autowired
    ITalCamaRepository camaRepository;

    @Autowired
    ModelMapper camaMapper;

    @Override
    public CreatedResponse registrarCama(RegistrarCamaRequest request) {
        try {
            log.info("[CamaServiceImpl][registrarCama][INICIO] Request: " + request);

            //Crear cama a guardar
            TalCama camaEntity = camaMapper.map(new Cama(null, request.getCodigo(), EstadoCama.DISPONIBLE.toString()), TalCama.class);
            log.info("[CamaServiceImpl][registrarCama] CamaEntity: " + camaEntity);

            //Guardar cama
            camaEntity = camaRepository.save(camaEntity);
            log.info("[CamaServiceImpl][registrarCama] Save CamaEntity: " + camaEntity);

            return new CreatedResponse(camaEntity.getId(), Utils.nowStringLDTAmericaSantiago());
        }catch (DataIntegrityViolationException e) {
            log.info("[CamaServiceImpl][registrarCama] Error: " + e);
            throw new BackwebBusinessException(BackwebExceptionCodes.BACKWEB_CAMA_0001);
        }
    }

    @Override
    public GetResponse obtenerCamas() {
        try {
            log.info("[CamaServiceImpl][obtenerCamas][INICIO]");
            List<TalCama> camas = camaRepository.findAll();
            log.info("[CamaServiceImpl][obtenerCamas] Camas: " + camas);

            Type listTypeCamas = new TypeToken<List<Cama>>() {}.getType();
            List<Cama> listaCamas = camaMapper.map(camas, listTypeCamas);
            log.info("[CamaServiceImpl][obtenerCamas][FIN] Return: " + listaCamas);
            return new GetResponse(listaCamas);
        }catch (Exception e) {
            log.info("[CamaServiceImpl][obtenerCamas] Error: " + e);
            throw new BackwebBusinessException(BackwebExceptionCodes.BACKWEB_CAMA_0002);
        }
    }

    @Override
    public GetResponse obtenerCamaById(Integer id) {
        try {
            log.info("[CamaServiceImpl][obtenerCamaById] INICIO");
            //Obtener Cama o lanzar excepcion si no encontramos
            TalCama camaEntity = camaRepository.findById(id).orElseThrow(NoCamasFoundException::new);
            log.info("[CamaServiceImpl][obtenerCamaById][FIN] Return: " + camaEntity);

            return new GetResponse(camaEntity);
        }catch (NoCamasFoundException e) {
            log.info("[CamaServiceImpl][obtenerCamaById] Error: " + e);
            throw new BackwebBusinessException(BackwebExceptionCodes.BACKWEB_CAMA_0003);
        }
    }

    @Override
    public GetResponse actualizarCamaById(ActualizarCamaRequest request) {
        try {
            log.info("[CamaServiceImpl][actualizarCamaById][INICIO] Request: " + request);
            //Obtener Cama o lanzar excepcion si no encontramos
            TalCama camaEntity = camaRepository.findById(request.getCama().getId()).orElseThrow(NoCamasFoundException::new);
            log.info("[CamaServiceImpl][actualizarCamaById] Cama a actualizar: " + camaEntity);

            //Actualizar Cama
            camaEntity = camaRepository.save(camaMapper.map(request.getCama(), TalCama.class));

            return new GetResponse(camaEntity);
        }catch (NoCamasFoundException e) {
            log.info("[CamaServiceImpl][actualizarCamaById] Error NoCamasFoundException: " + e);
            throw new BackwebBusinessException(BackwebExceptionCodes.BACKWEB_CAMA_0003);
        } catch (DataIntegrityViolationException e) {
            log.info("[CamaServiceImpl][actualizarCamaById] Error DataIntegrityViolationException: " + e);
            throw new BackwebBusinessException(BackwebExceptionCodes.BACKWEB_CAMA_0001);
        }
    }

    @Override
    public GetResponse eliminarCamaById(Integer id) {
        try {
            log.info("[CamaServiceImpl][eliminarCamaById][INICIO] ID: " + id);
            //Obtener Cama o lanzar excepcion si no encontramos
            camaRepository.deleteById(id);
            log.info("[CamaServiceImpl][eliminarCamaById][FIN] Cama eliminada correctamente.");

            return new GetResponse(id);
        } catch (DataIntegrityViolationException e) {
            log.info("[CamaServiceImpl][eliminarCamaById] Error DataIntegrityViolationException: " + e);
            throw new BackwebBusinessException(BackwebExceptionCodes.BACKWEB_CAMA_0001);
        }
    }
}
