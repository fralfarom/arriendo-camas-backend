package org.backweb.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.backweb.common.messaging.CreatedResponse;
import org.backweb.common.messaging.GetResponse;
import org.backweb.domain.dto.Cliente;
import org.backweb.domain.request.ActualizarClienteRequest;
import org.backweb.domain.request.RegistrarClienteRequest;
import org.backweb.exception.BackwebBusinessException;
import org.backweb.exception.BackwebExceptionCodes;
import org.backweb.persistence.entity.TalCliente;
import org.backweb.persistence.repository.ITalClienteRepository;
import org.backweb.service.IClienteService;
import org.backweb.service.exceptions.NoClientesFoundException;
import org.backweb.service.util.Utils;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Implementacion de servicios ClienteService
 *
 * @author FranciscoAlfaro
 */
@Slf4j
@Service
public class ClienteServiceImpl implements IClienteService {
    @Autowired
    ITalClienteRepository clienteRepository;

    @Autowired
    ModelMapper clienteMapper;
    @Override
    public CreatedResponse registrarCliente(RegistrarClienteRequest request) {
        try {
            log.info("[ClienteServiceImpl][obtenerClientes][INICIO] Request: " + request);

            //Crear Cliente a guardar
            TalCliente clienteEntity = clienteMapper.map(new Cliente(null, request.getRut(), request.getNombre(), request.getTelefono()), TalCliente.class);
            log.info("[ClienteServiceImpl][obtenerClientes] ClienteEntity: " + clienteEntity);

            //Guardar Cliente
            clienteEntity = clienteRepository.save(clienteEntity);
            log.info("[ClienteServiceImpl][obtenerClientes] Save ClienteEntity: " + clienteEntity);

            return new CreatedResponse(clienteEntity.getId(), Utils.nowStringLDTAmericaSantiago());
        }catch (DataIntegrityViolationException e) {
            log.info("[ClienteServiceImpl][obtenerClientes] Error: " + e);
            throw new BackwebBusinessException(BackwebExceptionCodes.BACKWEB_CLIENTE_0001);
        }
    }

    @Override
    public GetResponse obtenerClientes() {
        try {
            log.info("[ClienteServiceImpl][obtenerClientes][INICIO]");
            List<TalCliente> clientes = clienteRepository.findAll();
            log.info("[ClienteServiceImpl][obtenerClientes] Clientes: " + clientes);

            Type listTypeClientes = new TypeToken<List<Cliente>>() {}.getType();
            List<Cliente> listaClientes = clienteMapper.map(clientes, listTypeClientes);
            log.info("[ClienteServiceImpl][obtenerClientes][FIN] Return: " + listaClientes);
            return new GetResponse(listaClientes);
        }catch (Exception e) {
            log.info("[ClienteServiceImpl][obtenerClientes] Error: " + e);
            throw new BackwebBusinessException(BackwebExceptionCodes.BACKWEB_CLIENTE_0002);
        }
    }

    @Override
    public GetResponse obtenerClienteById(Integer id) {
        try {
            log.info("[ClienteServiceImpl][obtenerClienteById] ID: " + id);
            //Obtener Cliente o lanzar excepcion si no encontramos
            TalCliente clienteEntity = clienteRepository.findById(id).orElseThrow(NoClientesFoundException::new);
            log.info("[ClienteServiceImpl][obtenerClienteById][FIN] Return: " + clienteEntity);

            return new GetResponse(clienteEntity);
        }catch (NoClientesFoundException e) {
            log.info("[ClienteServiceImpl][obtenerClienteById] Error: " + e);
            throw new BackwebBusinessException(BackwebExceptionCodes.BACKWEB_CLIENTE_0003);
        }
    }

    @Override
    public GetResponse actualizarClienteById(ActualizarClienteRequest request) {
        try {
            log.info("[ClienteServiceImpl][actualizarClienteById] Request: " + request);
            //Obtener Cliente o lanzar excepcion si no encontramos
            TalCliente clienteEntity = clienteRepository.findById(request.getCliente().getId()).orElseThrow(NoClientesFoundException::new);
            log.info("[ClienteServiceImpl][actualizarClienteById] ClienteEntity: " + clienteEntity);

            //Actualizar Cliente
            clienteEntity = clienteRepository.save(clienteMapper.map(request.getCliente(), TalCliente.class));

            return new GetResponse(clienteEntity);
        }catch (NoClientesFoundException e) {
            log.info("[ClienteServiceImpl][actualizarClienteById] Error NoClientesFoundException: " + e);
            throw new BackwebBusinessException(BackwebExceptionCodes.BACKWEB_CLIENTE_0003);
        } catch (DataIntegrityViolationException e) {
            log.info("[ClienteServiceImpl][actualizarClienteById] Error DataIntegrityViolationException: " + e);
            throw new BackwebBusinessException(BackwebExceptionCodes.BACKWEB_CLIENTE_0001);
        }
    }

    @Override
    public GetResponse eliminarClienteById(Integer id) {
        try {
            log.info("[ClienteServiceImpl][eliminarClienteById][INICIO] ID: " + id);
            //Obtener Cama o lanzar excepcion si no encontramos
            clienteRepository.deleteById(id);
            log.info("[ClienteServiceImpl][eliminarClienteById][FIN] Cliente eliminado correctamente.");

            return new GetResponse(id);
        } catch (DataIntegrityViolationException e) {
            log.info("[ClienteServiceImpl][eliminarClienteById] Error DataIntegrityViolationException: " + e);
            throw new BackwebBusinessException(BackwebExceptionCodes.BACKWEB_CAMA_0001);
        }
    }
}
