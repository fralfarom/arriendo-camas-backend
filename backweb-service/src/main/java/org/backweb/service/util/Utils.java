package org.backweb.service.util;

import org.backweb.common.constant.Constantes;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Formatter;
import java.util.Optional;

/**
 * Clase Utils para funciones de utilidad dentro de la aplicacion.
 */
public final class Utils {

    private Utils() {
        super();
    }

    /**
     * Now string ldt america santiago string.
     *
     * @return una cadena de texto
     */
    public static String nowStringLDTAmericaSantiago() {

        ZoneId zid = ZoneId.of("America/Santiago");

        return String.valueOf(LocalDateTime.now(zid));

    }

    /**
     * Now ldt america santiago local date time.
     *
     * @return local date time
     */
    public static LocalDateTime nowLDTAmericaSantiago() {

        ZoneId zid = ZoneId.of("America/Santiago");

        return LocalDateTime.now(zid);

    }

    /**
     * Get dv.
     *
     * @param rut el rut
     * @return el dv
     */
    public static String getDv(Long rut) {


        Long m = 0l;
        Long s = 1l;
        for (; rut != 0; rut /= 10) {
            s = (s + rut % 10 * (9 - m++ % 6)) % 11;
        }

        return (s > 0) ? String.valueOf(s - 1) : "K";
    }

    /**
     * Get ruc con dv.
     *
     * @param ruc el ruc
     * @return el ruc con dv
     */
    public static String getRucConDv(Long ruc) {

        Long rucAux = ruc;
        Long m = 0l;
        Long s = 1l;
        for (; ruc != 0; ruc /= 10) {
            s = (s + ruc % 10 * (9 - m++ % 6)) % 11;
        }

        String dv = (s > 0) ? String.valueOf(s - 1) : "K";
        Formatter formatter = new Formatter();
        String rucConDv = String.format("%010d", rucAux).concat("-").concat(dv);
        formatter.close();

        return rucConDv;
    }

    /**
     * Compara ruc.
     *
     * @param ruc1 el ruc 1
     * @param ruc2 el ruc 2
     * @return valor boolean
     */
    public static boolean compareRuc(String ruc1, String ruc2) {
        if (ruc1 == null || ruc2 == null) {
            return Boolean.FALSE;
        }
        return ruc1.equalsIgnoreCase(ruc2);
    }

    /**
     * Is null or blank boolean.
     *
     * @param value el valor
     * @return valor boolean
     */
    public static boolean isNullOrBlank(String value) {

        return (!Optional.ofNullable(value).isPresent() || Constantes.BLANK.equalsIgnoreCase(value));
    }

}
